package ru.hh.school.lms;

@FunctionalInterface
public interface MessageManager {

  String of(String messageCode, Object... args);

}
