package ru.hh.school.lms.demo;

import com.github.seratch.jslack.api.model.Conversation;
import com.github.seratch.jslack.api.model.User;
import com.github.seratch.jslack.api.model.block.DividerBlock;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.element.ImageElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.slack.SlackHelper;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.time.LocalDate.now;

@Path("/demo")
public final class NotificationsResource {
  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsResource.class);

  private static final String DEADLINE_NOTIFICATION_TYPE = "deadlines";
  private static final String NEXT_WEEK_LECTURES_NOTIFICATION_TYPE = "lectures";
  private static final String TWO_WEEK_LATER_LECTURES_NOTIFICATION_TYPE = "drives";

  private final String adminChannelId;

  private final SlackHelper slack;
  private final HomeworkDao homeworkDao;
  private final LectureDao lectureDao;
  private final Function<LocalDate, String> dateFormatter;

  @Autowired
  public NotificationsResource(SlackHelper slack,
                               HomeworkDao homeworkDao,
                               LectureDao lectureDao,
                               @Qualifier("dayOfWeek") Function<LocalDate, String> dateFormatter,
                               @Qualifier("serviceProperties") Properties settings) {
    this.slack = slack;
    this.homeworkDao = homeworkDao;
    this.lectureDao = lectureDao;
    this.dateFormatter = dateFormatter;
    String adminChannelName = settings.getProperty("ru.hh.school.lms.role.admin.channel");
    this.adminChannelId = slack.getConversationByName(adminChannelName)
      .orElseThrow(() -> new RuntimeException("Can't get general channel which is called " + adminChannelName +
        "Please, create a channel with specified name.")).getId();
  }

  @POST
  @Path("/notify")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(MediaType.APPLICATION_JSON)
  public Response processSlashCommand(@BeanParam DemoEvent event) {
    Optional<Conversation> channel = slack.getConversationByName(event.getChannelName());
    if (channel.isEmpty()) {
      return Response.status(404, "No channel with name: " + event.getChannelName()).build();
    }
    switch (event.getTypeOfNotification()) {
      case DEADLINE_NOTIFICATION_TYPE:
        slack.post(channel.get().getId(), "", deadlineBlocks());
        break;
      case NEXT_WEEK_LECTURES_NOTIFICATION_TYPE:
        slack.post(event.getChannelName(), "", lecturesBlock());
        break;
      case TWO_WEEK_LATER_LECTURES_NOTIFICATION_TYPE:
        slack.post(event.getChannelName(), "", drivesBlock());
        break;
    }
    LOGGER.info("Catch demo event. Notify.");
    return Response.ok().build();
  }

  //  @Scheduled(cron="0 30 10 * * FRI")
  private List<LayoutBlock> deadlineBlocks() {
    List<LayoutBlock> homeworks = new ArrayList<>();
    SectionBlock infoSection = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(":information_source: Напоминаем о ближайших *дедлайнах*")
        .build())
      .build();
    homeworks.add(infoSection);
//    TODO: choose a way to limit the interval
//    LocalDate now = now();
//    int daysToSaturday = DayOfWeek.SATURDAY.getValue() - DayOfWeek.from(now).getValue();
//    LocalDate nextMonday = now.plusDays(daysToSaturday + 1);
//    LocalDate afterNextMonday  = nextMonday.plusDays(DAYS_IN_WEEK);
//            .filter(homework -> homework.getDeadline().isAfter(nextMonday) &&
//      homework.getDeadline().isBefore(afterNextMonday)) // filter past deadlines
    homeworks.addAll(
      homeworkDao.getHomeworksSortedByDeadline().stream()
        .filter(homework -> sameWeek(homework.getDeadline(), now().plusDays(7))) // filter past deadlines
        .map(this::homeworkToLayoutBlocks)
        .flatMap(List::stream)
        .collect(Collectors.toList())
    );
    return homeworks;
  }

  private List<LayoutBlock> homeworkToLayoutBlocks(Homework homework) {
    String homeworkField = homework.getTaskLink().isEmpty() ?
      homework.getLecture().getTitle() : // only title if there is no link
      String.format("<%s|*%s*>", homework.getTaskLink(), homework.getLecture().getTitle()); // title with internal link
    List<String> values = List.of(
      homeworkField,
      dateFormatter.apply(homework.getDeadline())
    );
    return Arrays.asList(
      DividerBlock.builder().build(),
      SectionBlock.builder()
        .fields(
          values.stream().map(v ->
            MarkdownTextObject.builder().text(v).build()
          ).collect(Collectors.toList())
        )
        .build()
    );
  }

  //  @Scheduled(cron="0 30 10 * * FRI")
  private List<LayoutBlock> lecturesBlock() {
    List<LayoutBlock> homeworks = new ArrayList<>();
    SectionBlock infoSection = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(":information_source: *Лекции* на следующей неделе")
        .build())
      .build();
    homeworks.add(infoSection);
    homeworks.addAll(
      lectureDao.getLecturesSorted().stream()
        .filter(lecture -> sameWeek(lecture.getDate(), now().plusDays(7))) // next lectures
        .map(lecture -> lectureToLayoutBlocks(lecture, true))
        .flatMap(List::stream)
        .collect(Collectors.toList())
    );
    return homeworks;
  }

  private List<LayoutBlock> lectureToLayoutBlocks(Lecture lecture, Boolean isHWnote) {
    Optional<User> lecturerOpt = slack.getUserByEmail(lecture.getLecturer().getEmail());
    // slack id or full name
    String lecturer = lecturerOpt.isEmpty() ? lecture.getLecturer().getFullName() : String.format("<@%s>", lecturerOpt.get().getId());
    return Arrays.asList(
      DividerBlock.builder().build(),
      SectionBlock.builder()
        .text(MarkdownTextObject.builder()
          .text(String.format("*%s*", lecture.getTitle()) + "\n" +
            dateFormatter.apply(lecture.getDate()) + "\n" +
            lecturer +
            (lecture.isHw() && isHWnote ? "\nДЗ будет" : "")
          ).build())
        .accessory(lecturerOpt.map(user -> ImageElement.builder()
          .imageUrl(user.getProfile().getImage192())
          .altText(lecture.getLecturer().getFullName())
          .build()).orElse(null))
        .build());
  }

//  @Scheduled(cron = "0 30 10 * * MON")
  private List<LayoutBlock> drivesBlock() {
    List<LayoutBlock> layoutBlocks = new ArrayList<>();
    List<String> bots = slack.getAllUsers().stream().filter(User::isBot).map(User::getId).collect(Collectors.toList());
    String admins = slack.getMembers(adminChannelId).stream()
      .filter(id -> !bots.contains(id))
      .map(id -> String.format("<@%s>", id))
      .collect(Collectors.joining(", "));
    SectionBlock infoSection = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(":information_source: Через две недели *Лекции*." +
          "\n" + admins + " помогите организовать прогоны.")
        .build())
      .build();
    layoutBlocks.add(infoSection);
    layoutBlocks.addAll(
      lectureDao.getLecturesSorted().stream()
        .filter(lecture -> sameWeek(lecture.getDate(), now().plusDays(14))) // next lectures
        .map(lecture -> lectureToLayoutBlocks(lecture, false))
        .flatMap(List::stream)
        .collect(Collectors.toList())
    );
    return layoutBlocks;
  }

  private Calendar toCalendar(LocalDate date) {
    Calendar c = Calendar.getInstance();
    c.set(Calendar.DAY_OF_YEAR, date.getDayOfYear());
    return c;
  }

  private boolean sameWeek(LocalDate date1, LocalDate date2) {
    Calendar c = toCalendar(date1);
    int year1 = c.get(Calendar.YEAR);
    int week1 = c.get(Calendar.WEEK_OF_YEAR);

    Calendar c1 = toCalendar(date2);
    int year2 = c1.get(Calendar.YEAR);
    int week2 = c1.get(Calendar.WEEK_OF_YEAR);

    return week1 == week2 && year1 == year2;
  }
}
