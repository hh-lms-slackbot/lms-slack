package ru.hh.school.lms.onboarding;

import com.github.seratch.jslack.api.methods.response.conversations.ConversationsOpenResponse;
import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Confirmation;
import com.github.seratch.jslack.api.model.Conversation;
import com.github.seratch.jslack.api.model.event.MemberJoinedChannelEvent;
import com.github.seratch.jslack.api.rtm.RTMClient;
import com.github.seratch.jslack.api.rtm.RTMEventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.InteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;

import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Component
public final class Onboarding extends RTMEventHandler<MemberJoinedChannelEvent> {

  private static final Logger LOGGER = LoggerFactory.getLogger(SlackHelper.class);
  private static final String CHANNEL_BUTTON = "channel";
  public static final String SEPARATOR = " ";
  public static final String DANGER_STYLE = "danger";
  private final SlackHelper slack;
  private final RTMClient slackClient;
  private final MessageManager message;
  private final CallbackDispatcher dispatcher;
  private final ApplicationContext thisContext;
  private final String selfName;
  private final Properties settings;
  private final String adminChannelName;
  private final String lecturerChannelName;
  private final String generalChannelName;
  private final String studentChannelName;
  private final Boolean enabled;
  private final String command;
  private final String addUserToChannelCallback;

  @Autowired
  public Onboarding(SlackHelper slack,
                    RTMClient slackClient,
                    @Qualifier("onboardingMessage") MessageManager message,
                    @Qualifier("serviceProperties") Properties settings,
                    CallbackDispatcher dispatcher,
                    ApplicationContext thisContext) {
    this.slack = slack;
    this.slackClient = slackClient;
    this.message = message;
    this.dispatcher = dispatcher;
    this.thisContext = thisContext;
    this.settings = settings;
    this.enabled = Boolean.valueOf(settings.getProperty("ru.hh.school.lms.onboarding"));
    this.adminChannelName = settings.getProperty("ru.hh.school.lms.role.admin.channel");
    this.lecturerChannelName = settings.getProperty("ru.hh.school.lms.role.lecturer.channel");
    this.studentChannelName = settings.getProperty("ru.hh.school.lms.role.student.channel");
    this.generalChannelName = settings.getProperty("ru.hh.school.lms.general.channel");
    checkChannels(List.of(adminChannelName, lecturerChannelName, studentChannelName, generalChannelName));
    this.command = settings.getProperty("ru.hh.school.lms.command.main");
    selfName = String.format("<@%s>", slackClient.getConnectedBotUser().getId());
    addUserToChannelCallback = staticId("add_user_to_channel");
    dispatcher.register(addUserToChannelCallback, this::addUserToChannel);
  }

  private void checkChannels(List<String> channelNames) {
    boolean error = false;
    String botName = slackClient.getConnectedBotUser().getName();
    for (String channelName : channelNames) {
      Optional<Conversation> conversation = slack.getConversationByName(channelName);
      if (conversation.isEmpty()) {
        LOGGER.error(String.format("Can't find a channel with a name '%s'. " +
          "Please, create this channel and add bot to it using '/invite @%s'", channelName, botName));
        error = true;
      }
    }
    if (error) {
      throw new RuntimeException(String.format("Some required channels are absent. " +
        "Please, create all channels and add bot to all of them using '/invite @%s' command.\n " +
        "Required channels are: %s", botName, channelNames));
    }
  }


  @Override
  public void handle(MemberJoinedChannelEvent event) {
    if (!enabled) {
      return;
    }
    Optional<Conversation> generalChannel = slack.getConversationByName(generalChannelName);
    if (generalChannel.isEmpty()) {
      throw new RuntimeException("Cant't get general channel. It name must be " + generalChannelName);
    }
    if (generalChannel.get().getId().equals(event.getChannel())) {
      String userId = event.getUser();
      greetUser(userId);
      notifyAdmins(userId);
    }
  }

  private void greetUser(String userId) {
    slack.postIm(userId, message.of("hello", selfName, command), emptyList());
  }

  private void notifyAdmins(String userId) {
    String user = String.format("<@%s>", userId);
    Optional<Conversation> adminChannel = slack.getConversationByName(adminChannelName);
    if (adminChannel.isEmpty()) {
      throw new RuntimeException("Cant't get admin channel. It name must be " + adminChannelName);
    }
    String notificationMessage = message.of("new_user_notification", user);
    Attachment attachment = Attachment.builder()
      .actions(List.of(
        channelToAction(userId, studentChannelName, false),
        channelToAction(userId, lecturerChannelName, false),
        channelToAction(userId, adminChannelName, true)))
      .text(message.of("invite_user_label"))
      .callbackId(addUserToChannelCallback)
      .build();
    slack.post(adminChannel.get().getId(), notificationMessage, List.of(attachment), null);
  }

  private <T extends InteractionEvent> void addUserToChannel(T t) {
    MessageInteractionEvent event = (MessageInteractionEvent) t;
    String value = event.getActions().get(0).getValue();
    String[] values = value.split(SEPARATOR);
    String userId = values[0];
    String channelName = values[1];
    Optional<Conversation> channel = slack.getConversationByName(channelName);
    Attachment newAttachment = event.getOriginalMessage().getAttachments().get(0);
    String newLine;
    if (channel.isEmpty()) {
      newLine = message.of("no_such_channel", channelName);
    } else {
      try {
        boolean alreadyInChannel = slack.inviteToChannel(userId, channel.get().getId());
        newLine = alreadyInChannel ?
          message.of("already_invited", userId, channelName) :
          message.of("invitation_succeed", event.getUser().getId(), channelName);
        List<Action> newActions = newAttachment.getActions().stream().filter(action -> !action.getValue().equals(value)).collect(Collectors.toList());
        newAttachment.setActions(newActions);
      } catch (EventHandlingException exc) {
        LOGGER.error(String.format("Exception during user @%s invitation to #%s", userId, channel), exc);
        newLine = message.of("cant_invite") + String.format(" (%s)", exc.getResponseErrorMessage());
        if (exc.getResponseErrorMessage().equals("cant_invite_self")) {
          newLine += " " + message.of("cant_invite_self");
        }
      }
    }
    String newAttachmentText = newAttachment.getText() + "\n" + newLine;
    newAttachment.setText(newAttachmentText);

    slack.update(event.getChannel().getId(), event.getMessageTs(),
      event.getOriginalMessage().getText(), List.of(newAttachment));
  }

  private Action channelToAction(String userId, String channelName, boolean adminConfirmation) {
    Action.ActionBuilder actionBuilder = Action.builder()
      .type(Action.Type.BUTTON)
      .name(CHANNEL_BUTTON)
      .text("#" + channelName)
      .value(userId + SEPARATOR + channelName);
    if (adminConfirmation) {
      actionBuilder
        .style(DANGER_STYLE)
        .confirm(Confirmation.builder()
          .title(message.of("admin_confirm_title"))
          .text(message.of("admin_confirm_text"))
          .dismiss_text(message.of("admin_confirm_no"))
          .ok_text(message.of("admin_confirm_yes"))
          .build());
    }
    return actionBuilder.build();
  }

  //@EventListener
  public void checkNewMembers(ContextRefreshedEvent event) {
    if (!enabled || !thisContext.equals(event.getApplicationContext())) {
      return;
    }
    sendOnboarding(true);
  }

  public void sendOnboarding(boolean newOnly) {
    Optional<Conversation> generalChannel = slack.getConversationByName(generalChannelName);
    if (generalChannel.isEmpty()) {
      throw new RuntimeException("Can't get general channel. it name must be " + generalChannelName);
    }
    Set<String> generalMembersIds = slack.getMembers(generalChannel.get().getId());
    for (String generalMemberId : generalMembersIds) {
      try {
        ConversationsOpenResponse response = slack.openIm(generalMemberId, true);
        if (response.isAlreadyOpen() && newOnly) {
          continue;
        }
      } catch (EventHandlingException exc) {
        if (exc.getResponseErrorMessage().equals("cannot_dm_bot")) {
          continue;
        } else {
          throw exc;
        }
      }
      greetUser(generalMemberId);
      notifyAdmins(generalMemberId);
    }
  }

  private String staticId(String name) {
    return String.format("%s.%s", this.getClass().getCanonicalName(), name);
  }
}
