package ru.hh.school.lms.onboarding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import ru.hh.school.lms.MessageManager;

import java.util.Locale;

@Configuration
class OnboardingConfig {

  @Autowired
  private Locale locale;

  @Bean(name = "onboardingMessage")
  public MessageManager message() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("onboarding/messages", "onboarding/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return (messageCode, args) -> messageSource.getMessage(messageCode, args, locale);
  }

}
