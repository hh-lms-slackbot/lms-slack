package ru.hh.school.lms.slack.interactionexception;

import ru.hh.school.lms.slack.event.EventHandlingException;

public class InteractionException extends EventHandlingException {

  final private Error error;

  public InteractionException(String message, Error error) {
    super(message);
    this.error = error;
  }

  public Error getError() {
    return error;
  }
}
