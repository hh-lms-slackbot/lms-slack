package ru.hh.school.lms.slack;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.methods.MethodsClient;
import com.github.seratch.jslack.api.methods.impl.MethodsClientImpl;
import com.github.seratch.jslack.api.rtm.RTMClient;
import com.github.seratch.jslack.common.http.SlackHttpClient;
import com.github.seratch.jslack.shortcut.Shortcut;
import com.github.seratch.jslack.shortcut.model.ApiToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Properties;

@Configuration
public class SlackConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(SlackConfig.class);

  @Bean
  public SlackHttpClient slackHttpClient() {
    return new SlackHttpClient();
  }

  @Bean
  public Slack slack(SlackHttpClient slackHttpClient) {
    return Slack.getInstance(slackHttpClient);
  }

  @Bean
  public MethodsClient methodsClient(SlackHttpClient slackHttpClient) {
    return new MethodsClientImpl(slackHttpClient);
  }

  @Bean
  public SlackHelper slackHelper(
    @Qualifier("serviceProperties") Properties properties,
    MethodsClient methodsClient,
    SlackHttpClient httpClient
  ) {
    return new SlackHelper(properties, methodsClient, httpClient);
  }

  @Bean
  public Shortcut shortcut(@Qualifier("serviceProperties") Properties properties, Slack slack) {
    String slackBotToken = properties.getProperty("ru.hh.school.lms.slack.botToken");
    return slack.shortcut(ApiToken.of(slackBotToken));
  }

  @Bean
  public RTMClient client(@Qualifier("serviceProperties") Properties properties, Slack slack) throws IOException {
    RTMClient client = slack.rtmConnect(properties.getProperty("ru.hh.school.lms.slack.botToken"));
    client.addMessageHandler(message -> LOGGER.info("RTM message: " + message));
    client.addCloseHandler(reason -> LOGGER.info("RTM connection has been closed"));
    client.addErrorHandler(reason -> LOGGER.error("An error occurred during RTM session: ", reason));
    return client;
  }
}
