package ru.hh.school.lms.slack.event;

import com.github.seratch.jslack.api.model.Message;
import ru.hh.school.lms.slack.blockaction.BlockAction;

import java.util.List;

public final class BlockActionEvent extends InteractionEvent {

  private String apiAppId;
  private String token;
  private String triggerId;
  private String responseUrl;
  private Message message;
  private List<BlockAction> actions;

  @Override
  public String getCallbackId() {
    if (actions.isEmpty()) {
      return null;
    }
    return actions.get(0).getActionId();
  }

  public Message getMessage() {
    return message;
  }

  public String getApiAppId() {
    return apiAppId;
  }

  public String getToken() {
    return token;
  }

  public String getTriggerId() {
    return triggerId;
  }

  public String getResponseUrl() {
    return responseUrl;
  }

  public List<BlockAction> getActions() {
    return actions;
  }
}
