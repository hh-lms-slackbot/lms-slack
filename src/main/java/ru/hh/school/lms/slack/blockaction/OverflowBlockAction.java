package ru.hh.school.lms.slack.blockaction;

public final class OverflowBlockAction extends BlockAction {

  private SelectedOption selectedOption;

  public SelectedOption getSelectedOption() {
    return selectedOption;
  }

  @Override
  public String toString() {
    return "OverflowSelectBlockAction{" +
      "selectedOption=" + selectedOption +
      '}';
  }
}
