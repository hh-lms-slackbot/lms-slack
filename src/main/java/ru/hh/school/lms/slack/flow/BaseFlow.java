package ru.hh.school.lms.slack.flow;

import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;
import ru.hh.school.lms.slack.event.InteractionEvent;

import java.util.UUID;
import java.util.function.BiConsumer;

public class BaseFlow {

  private final CallbackDispatcher dispatcher;

  public BaseFlow(
    CallbackDispatcher dispatcher
  ) {
    this.dispatcher = dispatcher;
  }

  protected <T extends BaseFlowContext, E extends InteractionEvent> String attachCallback(String name, T context, BiConsumer<T, E> callback) {
    String callbackId = generateId(name);
    context.addCallbackId(callbackId);
    dispatcher.register(callbackId, context, callback);
    return callbackId;
  }

  protected <T extends BaseFlowContext> void dropContext(T context) {
    context
      .getCallbackIds()
      .forEach(dispatcher::deregister);
  }

  protected String generateId(String name) {
    return String.format("%s.%s#%s",
      this.getClass().getCanonicalName(),
      name,
      UUID.randomUUID()
    );
  }
}
