package ru.hh.school.lms.slack.event;

import com.github.seratch.jslack.api.model.Channel;
import com.github.seratch.jslack.api.model.Team;
import com.github.seratch.jslack.api.model.User;

public class InteractionEvent extends Event {
  private String callbackId;

  private Team team;
  private Channel channel;
  private User user;

  public String getCallbackId() {
    return callbackId;
  }

  public Team getTeam() {
    return team;
  }

  public Channel getChannel() {
    return channel;
  }

  public User getUser() {
    return user;
  }

  @Override
  public String toString() {
    return "InteractionEvent{" +
      "callbackId='" + callbackId + '\'' +
      ", team=" + team +
      ", channel=" + channel +
      ", user=" + user +
      '}';
  }
}
