package ru.hh.school.lms.slack;

import com.github.seratch.jslack.api.model.event.Event;
import com.github.seratch.jslack.api.rtm.RTMClient;
import com.github.seratch.jslack.api.rtm.RTMEventHandler;
import com.github.seratch.jslack.api.rtm.RTMEventsDispatcher;
import com.github.seratch.jslack.api.rtm.RTMEventsDispatcherFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.util.List;

@Component
final class SlackStarter {

  private final AnnotationConfigWebApplicationContext thisContext;
  private final RTMClient client;
  private final List<RTMEventHandler<? extends Event>> rtmHandlers;

  public SlackStarter(AnnotationConfigWebApplicationContext thisContext,
                      RTMClient client,
                      List<RTMEventHandler<? extends Event>> rtmHandlers) {
    this.thisContext = thisContext;
    this.client = client;
    this.rtmHandlers = rtmHandlers;
  }

  @EventListener
  public void startRtmClient(ContextRefreshedEvent event) throws IOException, DeploymentException {
    final ApplicationContext applicationContext = event.getApplicationContext();
    if (!thisContext.equals(applicationContext)) {
      return;
    }
    RTMEventsDispatcher dispatcher = RTMEventsDispatcherFactory.getInstance();
    for (RTMEventHandler<? extends Event> handler : rtmHandlers) {
      dispatcher.register(handler);
    }
    client.addMessageHandler(dispatcher.toMessageHandler());
    client.connect();
  }
}
