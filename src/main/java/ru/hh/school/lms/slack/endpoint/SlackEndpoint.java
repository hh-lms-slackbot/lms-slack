package ru.hh.school.lms.slack.endpoint;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.hh.school.lms.slack.event.BlockActionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.InteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.SlashCommandEvent;
import ru.hh.school.lms.slack.event.SlashCommandHandler;
import ru.hh.school.lms.slack.interactionexception.InteractionException;
import ru.hh.school.lms.slack.security.Secured;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/")
@Secured
public final class SlackEndpoint {

  private static final Logger LOGGER = LoggerFactory.getLogger(SlackEndpoint.class);

  private final List<SlashCommandHandler> commandHandlers;
  private final Gson gson;
  private final CallbackDispatcher dispatcher;

  @Autowired
  SlackEndpoint(List<SlashCommandHandler> commandHandlers,
                Gson gson,
                CallbackDispatcher dispatcher) {
    this.commandHandlers = commandHandlers;
    this.gson = gson;
    this.dispatcher = dispatcher;
  }

  @POST
  @Path("/slash-command")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  public Response processSlashCommand(@BeanParam SlashCommandEvent command) {
    try {
      for (SlashCommandHandler h : commandHandlers) {
        h.onEvent(command);
      }
      return Response.ok().build();
    } catch (EventHandlingException exc) {
      LOGGER.error("Error during command processing. Command:" + command.toString(), exc);
      return Response.serverError().build();
    }
  }

  @POST
  @Path("/interactive-component")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  public Response processInteractiveComponent(@FormParam("payload") String payload) {
    InteractionEvent event = gson.fromJson(payload, InteractionEvent.class);
    try {
      switch (event.getType()) {
        case DIALOG_SUBMISSION:
          event = gson.fromJson(payload, DialogSubmissionEvent.class);
          break;
        case INTERACTIVE_MESSAGE:
          event = gson.fromJson(payload, MessageInteractionEvent.class);
          break;
        case BLOCK_ACTIONS:
          event = gson.fromJson(payload, BlockActionEvent.class);
          break;
        default:
          LOGGER.warn("Unknown event type: " + event.getType() + " " + event.toString());
          return Response.ok().build();
      }
      dispatcher.dispatch(event);
      return Response.ok().build();
    } catch (InteractionException error) {
      String errorToJson = gson.toJson(error.getError());
      return Response.ok(errorToJson).build();
    } catch (EventHandlingException exc) {
      LOGGER.error("Failed to handle component interaction: payload=" + payload, exc);
      return Response.serverError().build();
    }
  }
}
