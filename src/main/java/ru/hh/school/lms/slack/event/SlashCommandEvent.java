package ru.hh.school.lms.slack.event;

import javax.ws.rs.FormParam;

/**
 * POJO that contains all parameters
 * passed by slack server with a POST endpoint request
 */
public final class SlashCommandEvent extends Event {

  @FormParam("token")
  private String token;

  @FormParam("team_id")
  private String teamId;

  @FormParam("team_domain")
  private String teamDomain;

  @FormParam("enterprise_id")
  private String enterpriseId;

  @FormParam("enterprise_name")
  private String enterpriseName;

  @FormParam("channel_id")
  private String channelId;

  @FormParam("channel_name")
  private String channelName;

  @FormParam("user_id")
  private String userId;

  @FormParam("user_name")
  private String userName;

  @FormParam("command")
  private String command;

  @FormParam("text")
  private String text;

  @FormParam("response_url")
  private String responseUrl;

  @FormParam("trigger_id")
  private String triggerId;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getTeamId() {
    return teamId;
  }

  public void setTeamId(String teamId) {
    this.teamId = teamId;
  }

  public String getTeamDomain() {
    return teamDomain;
  }

  public void setTeamDomain(String teamDomain) {
    this.teamDomain = teamDomain;
  }

  public String getEnterpriseId() {
    return enterpriseId;
  }

  public void setEnterpriseId(String enterpriseId) {
    this.enterpriseId = enterpriseId;
  }

  public String getEnterpriseName() {
    return enterpriseName;
  }

  public void setEnterpriseName(String enterpriseName) {
    this.enterpriseName = enterpriseName;
  }

  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public String getChannelName() {
    return channelName;
  }

  public void setChannelName(String channelName) {
    this.channelName = channelName;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getResponseUrl() {
    return responseUrl;
  }

  public void setResponseUrl(String responseUrl) {
    this.responseUrl = responseUrl;
  }

  public String getTriggerId() {
    return triggerId;
  }

  public void setTriggerId(String triggerId) {
    this.triggerId = triggerId;
  }

  @Override
  public String toString() {
    return "SlashCommandEvent{" +
      "token='" + token + '\'' +
      ", teamId='" + teamId + '\'' +
      ", teamDomain='" + teamDomain + '\'' +
      ", enterpriseId='" + enterpriseId + '\'' +
      ", enterpriseName='" + enterpriseName + '\'' +
      ", channelId='" + channelId + '\'' +
      ", channelName='" + channelName + '\'' +
      ", userId='" + userId + '\'' +
      ", userName='" + userName + '\'' +
      ", command='" + command + '\'' +
      ", text='" + text + '\'' +
      ", responseUrl='" + responseUrl + '\'' +
      ", triggerId='" + triggerId + '\'' +
      '}';
  }
}
