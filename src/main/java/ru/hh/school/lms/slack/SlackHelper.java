package ru.hh.school.lms.slack;

import com.github.seratch.jslack.api.methods.MethodsClient;
import com.github.seratch.jslack.api.methods.SlackApiResponse;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsCreateRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsJoinRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatDeleteRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatPostEphemeralRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatPostMessageRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatUpdateRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsHistoryRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsInfoRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsInviteRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsListRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsMembersRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsOpenRequest;
import com.github.seratch.jslack.api.methods.request.dialog.DialogOpenRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsCreateRequest;
import com.github.seratch.jslack.api.methods.request.im.ImOpenRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersConversationsRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersInfoRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersListRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersLookupByEmailRequest;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsCreateResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsJoinResponse;
import com.github.seratch.jslack.api.methods.response.channels.UsersLookupByEmailResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsHistoryResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsInfoResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsListResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsMembersResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsOpenResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsCreateResponse;
import com.github.seratch.jslack.api.methods.response.im.ImOpenResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersConversationsResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersInfoResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersListResponse;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Channel;
import com.github.seratch.jslack.api.model.Conversation;
import com.github.seratch.jslack.api.model.ConversationType;
import com.github.seratch.jslack.api.model.Group;
import com.github.seratch.jslack.api.model.Message;
import com.github.seratch.jslack.api.model.ResponseMetadata;
import com.github.seratch.jslack.api.model.User;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.app_backend.interactive_messages.response.ActionResponse;
import com.github.seratch.jslack.common.http.SlackHttpClient;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;

import static java.util.Collections.emptyList;

@Component
public final class SlackHelper {

  private static final Logger LOGGER = LoggerFactory.getLogger(SlackHelper.class);

  private final static String ALREADY_IN_CHANNEL_ERROR = "already_in_channel";
  private final static String USERS_NOT_FOUND = "users_not_found";
  private final static Integer DEFAULT_LIMIT = 50;

  private final String slackBotToken;
  private final String slackAppToken;
  private final MethodsClient methodsClient;
  private final SlackHttpClient httpClient;

  SlackHelper(
    Properties properties,
    MethodsClient methodsClient,
    SlackHttpClient httpClient
  ) {
    this.slackBotToken = properties.getProperty("ru.hh.school.lms.slack.botToken");
    this.slackAppToken = properties.getProperty("ru.hh.school.lms.slack.appToken");
    this.methodsClient = methodsClient;
    this.httpClient = httpClient;
  }

  public void post(String channelId, String responseMessage) {
    post(channelId, responseMessage, emptyList(), emptyList());
  }

  public void post(String channelId, List<LayoutBlock> blocks) {
    post(channelId, "", emptyList(), blocks);
  }

  public void post(String channelId, String responseMessage, List<LayoutBlock> blocks) {
    post(channelId, responseMessage, emptyList(), blocks);
  }

  public void post(String channelId, String responseMessage, List<Attachment> attachments, List<LayoutBlock> blocks) {
    tryDo(() -> {
      ChatPostMessageRequest request = ChatPostMessageRequest.builder()
        .token(slackBotToken)
        .channel(channelId)
        .text(responseMessage)
        .blocks(blocks)
        .attachments(attachments).build();
      return methodsClient.chatPostMessage(request);
    });
  }

  public void postIm(String userId, String message, List<LayoutBlock> blocks) {
    postIm(userId, message, emptyList(), blocks);
  }

  public void postIm(String userId, String message, List<Attachment> attachments, List<LayoutBlock> blocks) {
    ImOpenResponse imOpenResponse =
      tryDo(() -> methodsClient.imOpen(ImOpenRequest.builder()
        .token(slackBotToken)
        .user(userId)
        .build()));
    post(imOpenResponse.getChannel().getId(), message, attachments, blocks);
  }

  public void postEphemeral(String channelId, String user, String message, List<LayoutBlock> blocks) {
    postEphemeral(channelId, user, message, emptyList(), blocks);
  }

  public void postEphemeral(String channelId, String user, String message, List<Attachment> attachments, List<LayoutBlock> blocks) {
    tryDo(() -> {
      ChatPostEphemeralRequest request = ChatPostEphemeralRequest.builder()
        .token(slackBotToken)
        .text(message)
        .user(user)
        .channel(channelId)
        .attachments(attachments)
        .blocks(blocks)
        .build();
      return methodsClient.chatPostEphemeral(request);
    });
  }

  public void delete(String channel, String ts) {
    tryDo(() -> {
      ChatDeleteRequest chatDeleteRequest = ChatDeleteRequest.builder()
        .token(slackBotToken)
        .channel(channel)
        .ts(ts)
        .build();
      return methodsClient.chatDelete(chatDeleteRequest);
    });
  }

  public void update(String channel, String ts, String responseMessage, List<Attachment> attachments) {
    update(channel, ts, responseMessage, attachments, emptyList());
  }

  public void update(String channel, String ts, String responseMessage, List<Attachment> attachments, List<LayoutBlock> blocks) {
    tryDo(() -> {
      ChatUpdateRequest.ChatUpdateRequestBuilder builder = ChatUpdateRequest.builder()
        .token(slackBotToken)
        .channel(channel)
        .ts(ts)
        .text(responseMessage)
        .blocks(blocks)
        .attachments(attachments);
      return methodsClient.chatUpdate(builder.build());
    });
  }

  public void respond(String responseUrl, ActionResponse action) {
    Response response;
    try {
      response = httpClient.postJsonPostRequest(responseUrl, action);
    } catch (Exception exc) {
      throw new EventHandlingException("An exception occurred during slack interaction", exc);
    }

    if (!response.isSuccessful()) {
      throw new EventHandlingException("Response is not successful", response.message());
    }
  }

  public void dialog(String triggerId, Dialog dialog) throws EventHandlingException {
    tryDo(() -> {
      DialogOpenRequest request = DialogOpenRequest.builder()
        .token(slackBotToken)
        .triggerId(triggerId)
        .dialog(dialog)
        .build();
      return methodsClient.dialogOpen(request);
    });
  }

  public Optional<Conversation> getChannel(String channelId) {
    ConversationsInfoResponse infoResponse = tryDo(() -> methodsClient.conversationsInfo(ConversationsInfoRequest.builder()
      .channel(channelId)
      .token(slackBotToken)
      .build()
    ));
    return Optional.ofNullable(infoResponse.getChannel());
  }

  public Optional<Conversation> getConversationByName(String channelName) {
    Set<Conversation> conversations = getConversations(Set.of(ConversationType.PUBLIC_CHANNEL, ConversationType.PRIVATE_CHANNEL));
    return conversations.stream().filter(conversation -> channelName.equals(conversation.getName())).findFirst();
  }

  public Set<Conversation> getConversations(Set<ConversationType> types) {
    Set<Conversation> conversations = new HashSet<>();
    tryDo(() -> {
      String cursor = null;
      ConversationsListResponse response;
      do {
        ConversationsListRequest request = ConversationsListRequest.builder()
          .limit(DEFAULT_LIMIT)
          .token(slackBotToken)
          .types(List.copyOf(types))
          .build();
        if (cursor != null) {
          request.setCursor(cursor);
        }
        response = methodsClient.conversationsList(request);
        if (!response.isOk()) {
          throw new RuntimeException("Can't get conversations with given types: " + types);
        }
        conversations.addAll(response.getChannels());
        cursor = getCursor(response.getResponseMetadata());
      } while (cursor != null);
      return response;
    });
    return conversations;
  }

  public Set<String> getMembers(String channelId) {
    Set<String> members = new HashSet<>();
    tryDo(() -> {
      String cursor = null;
      ConversationsMembersResponse response;
      do {
        ConversationsMembersRequest request = ConversationsMembersRequest.builder()
          .token(slackBotToken)
          .channel(channelId)
          .limit(DEFAULT_LIMIT)
          .build();
        if (cursor != null) {
          request.setCursor(cursor);
        }
        response = methodsClient.conversationsMembers(request);
        if (!response.isOk()) {
          throw new RuntimeException("Can't get members for given channel: " + channelId);
        }
        members.addAll(response.getMembers());
        cursor = getCursor(response.getResponseMetadata());
      } while (cursor != null);
      return response;
    });
    return members;
  }

  public ConversationsOpenResponse openIm(String userId, boolean ignoreBot) {
    return tryDo(() -> methodsClient.conversationsOpen(ConversationsOpenRequest.builder()
      .token(slackBotToken)
      .returnIm(true)
      .users(List.of(userId))
      .build())
    );
  }

  public Set<Conversation> getPublicAndPrivateChannelsByUserId(String userId) {
    Set<Conversation> channels = new HashSet<>();
    tryDo(() -> {
      String cursor = null;
      UsersConversationsResponse response = methodsClient.usersConversations(conversationRequest(userId, cursor));
      if (!response.isOk()) {
        throw new RuntimeException("Can't get user conversations, userId=" + userId + ", error message: " + response.getError());
      }
      channels.addAll(response.getChannels());
      cursor = getCursor(response.getResponseMetadata());
      while (cursor != null) {
        response = methodsClient.usersConversations(conversationRequest(userId, cursor));
        channels.addAll(response.getChannels());
        cursor = getCursor(response.getResponseMetadata());
      }
      return response;
    });
    return channels;
  }

  public Optional<User> getUserByEmail(String email) {
    UsersLookupByEmailResponse response = tryDo(() ->
      methodsClient.usersLookupByEmail(
        UsersLookupByEmailRequest.builder()
          .token(slackBotToken)
          .email(email)
          .build()
      ), USERS_NOT_FOUND);
    return Optional.ofNullable(response.getUser());
  }

  public List<User> getAllUsers() {
    List<User> resultUserList = new ArrayList<>();
    String cursor = null;
    do {
      UsersListRequest request = UsersListRequest.builder()
        .limit(DEFAULT_LIMIT)
        .cursor(cursor)
        .token(slackBotToken)
        .build();

      UsersListResponse response = tryDo(() -> methodsClient.usersList(request));
      resultUserList.addAll(response.getMembers());
      cursor = getCursor(response.getResponseMetadata());
    } while (cursor != null);
    return resultUserList;
  }

  public Optional<User> getUser(String userId) {
    UsersInfoResponse usersInfoResponse = tryDo(() -> methodsClient.usersInfo(UsersInfoRequest.builder()
      .token(slackBotToken)
      .user(userId)
      .build()));
    return Optional.ofNullable(usersInfoResponse.getUser());
  }

  private List<Message> getLatestMessages(String channel, String ts, int limit) throws EventHandlingException {
    ConversationsHistoryResponse historyResponse = tryDo(() -> {
      ConversationsHistoryRequest request = ConversationsHistoryRequest.builder()
        .token(slackBotToken)
        .channel(channel)
        .inclusive(true)
        .oldest(ts)
        .build();
      if (limit > 0) {
        request.setLimit(1);
      }
      return methodsClient.conversationsHistory(request);
    });
    return historyResponse.getMessages();
  }

  public Optional<Message> getSingleMessage(String channel, String ts) {
    List<Message> latestMessages = getLatestMessages(channel, ts, 1);
    return latestMessages.isEmpty() ? Optional.empty() : Optional.of(latestMessages.get(0));
  }

  public Optional<Channel> createChannel(String name) {
    ChannelsCreateResponse response = tryDo(() -> methodsClient.channelsCreate(ChannelsCreateRequest.builder()
      .token(slackAppToken)
      .name(name)
      .build()));
    return Optional.ofNullable(response.getChannel());
  }

  public Optional<Group> createGroup(String name) {
    GroupsCreateResponse response = tryDo(() -> methodsClient.groupsCreate(GroupsCreateRequest.builder()
      .token(slackAppToken)
      .name(name)
      .build()));
    return Optional.ofNullable(response.getGroup());
  }

  public Optional<Channel> joinChannel(String channelName) {
    ChannelsJoinResponse response = tryDo(() -> methodsClient.channelsJoin(ChannelsJoinRequest.builder()
      .token(slackAppToken)
      .name(channelName)
      .build()));
    return Optional.ofNullable(response.getChannel());
  }

  public boolean inviteToChannel(String userId, String channelId) {
    try {
      tryDo(() -> methodsClient.conversationsInvite(ConversationsInviteRequest.builder()
        .channel(channelId)
        .token(slackAppToken)
        .users(List.of(userId))
        .build()));
    } catch (EventHandlingException exc) {
      String errorMessage = exc.getResponseErrorMessage();
      if (errorMessage != null && errorMessage.equals(ALREADY_IN_CHANNEL_ERROR)) {
        LOGGER.warn(ALREADY_IN_CHANNEL_ERROR + " has been ignored; userId=" + userId + " channelId=" + channelId);
        return true; // already in channel
      }
      throw exc;
    }
    return false; // newly invited
  }

  private <T extends SlackApiResponse> T tryDo(Callable<T> callable) {
    T response = null;
    try {
      response = callable.call();
    } catch (Exception exc) {
      throw new EventHandlingException("An exception occurred during slack interaction", exc);
    }
    if (response != null && !response.isOk()) {
      throw new EventHandlingException("SlackApiResponse is not Ok.", response.getError());
    }
    return response;
  }

  private <T extends SlackApiResponse> T tryDo(Callable<T> callable, String handledErrorMessage) {
    T response = null;
    try {
      response = callable.call();
    } catch (Exception exc) {
      throw new EventHandlingException("An exception occurred during slack interaction", exc);
    }
    if (!response.isOk() && !Objects.equals(response.getError(), handledErrorMessage)) {
      throw new EventHandlingException("SlackApiResponse is not Ok.", response.getError());
    }
    return response;
  }

  private String getCursor(ResponseMetadata metadata) {
    if (metadata.getNextCursor() == null || metadata.getNextCursor().isEmpty()) {
      return null;
    }
    return metadata.getNextCursor();
  }

  private UsersConversationsRequest conversationRequest(String userId, String cursor) {
    UsersConversationsRequest request = UsersConversationsRequest.builder()
      .limit(DEFAULT_LIMIT)
      .excludeArchived(false)
      .token(slackBotToken)
      .user(userId)
      .types(List.of(ConversationType.PUBLIC_CHANNEL, ConversationType.PRIVATE_CHANNEL))
      .build();
    if (cursor != null) {
      request.setCursor(cursor);
    }
    return request;
  }
}
