package ru.hh.school.lms.slack.event;

import com.google.gson.annotations.SerializedName;

import javax.ws.rs.FormParam;
import java.util.Map;

public final class DialogSubmissionEvent extends InteractionEvent {
  @FormParam("token")
  private String token;
  @FormParam("action_ts")
  @SerializedName("action_ts")
  private String actionTs;
  @SerializedName("response_url")
  private String responseUrl;
  private String state;
  private Map<String, String> submission;

  public Map<String, String> getSubmission() {
    return submission;
  }

  public void setSubmission(Map<String, String> submission) {
    this.submission = submission;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getActionTs() {
    return actionTs;
  }

  public void setActionTs(String actionTs) {
    this.actionTs = actionTs;
  }

  public String getResponseUrl() {
    return responseUrl;
  }

  public void setResponseUrl(String responseUrl) {
    this.responseUrl = responseUrl;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "DialogSubmissionEvent{" +
      "token='" + token + '\'' +
      ", actionTs='" + actionTs + '\'' +
      ", responseUrl='" + responseUrl + '\'' +
      ", state='" + state + '\'' +
      '}';
  }
}
