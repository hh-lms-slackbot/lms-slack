package ru.hh.school.lms.slack.event;

import static ru.hh.school.lms.slack.event.EventType.UNKNOWN;

public class Event {

  private EventType type;

  public EventType getType() {
    return type == null ? UNKNOWN : type;
  }

  public void setType(EventType type) {
    this.type = type;
  }

}
