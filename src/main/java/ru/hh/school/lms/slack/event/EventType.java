package ru.hh.school.lms.slack.event;

import com.google.gson.annotations.SerializedName;

public enum EventType {
  @SerializedName("dialog_submission")
  DIALOG_SUBMISSION,
  @SerializedName("interactive_message")
  INTERACTIVE_MESSAGE,
  @SerializedName("block_actions")
  BLOCK_ACTIONS,
  UNKNOWN
}
