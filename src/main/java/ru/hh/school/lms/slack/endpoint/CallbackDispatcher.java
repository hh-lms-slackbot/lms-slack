package ru.hh.school.lms.slack.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.slack.event.EventHandler;
import ru.hh.school.lms.slack.event.InteractionEvent;
import ru.hh.school.lms.slack.flow.BaseFlowContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;

@Component
public class CallbackDispatcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(CallbackDispatcher.class);

  private final Map<String, EventHandler<? extends InteractionEvent>> callbacks = new ConcurrentHashMap<>();

  public <T extends InteractionEvent> void register(String callbackId, EventHandler<T> callback) {
    callbacks.put(callbackId, callback);
  }

  public <T extends InteractionEvent, D extends BaseFlowContext> void register(String id, D context, BiConsumer<D, T> handler) {
    register(id, (T e) -> {
      synchronized (context) {
        handler.accept(context, e);
      }
    });
  }

  public boolean deregister(String callbackId) {
    return callbacks.remove(callbackId) != null;
  }

  void dispatch(InteractionEvent event) {
    EventHandler eventHandler = callbacks.get(event.getCallbackId());
    if (eventHandler == null) {
      LOGGER.error("Callback not found: " + event.getCallbackId() + " " + event.toString());
      return;
    }
    try {
      eventHandler.onEvent(event);
    } catch (ClassCastException exc) {
      LOGGER.error("You registered wrong callback function for id: " + event.getCallbackId() + " it must process " + event.getType() + " events");
    }
  }
}
