package ru.hh.school.lms.slack.blockaction;

public final class StaticSelectBlockAction extends BlockAction {

  private SelectedOption selectedOption;

  public SelectedOption getSelectedOption() {
    return selectedOption;
  }

  @Override
  public String toString() {
    return "StaticSelectBlockAction{" +
      "selectedOption=" + selectedOption +
      '}';
  }
}
