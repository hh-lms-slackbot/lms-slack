package ru.hh.school.lms.slack.blockaction;

import com.github.seratch.jslack.api.model.block.composition.TextObject;

public final class ButtonBlockAction extends BlockAction {

  private TextObject text;

  public TextObject getText() {
    return text;
  }

  @Override
  public String toString() {
    return "ButtonBlockAction{" +
      "text=" + text +
      '}';
  }
}
