package ru.hh.school.lms.slack.blockaction;

public final class DatePickerButtonAction extends BlockAction {

  private final String selectedDate;
  private final String initialDate;

  public DatePickerButtonAction(String selectedDate, String initialDate) {
    this.selectedDate = selectedDate;
    this.initialDate = initialDate;
  }

  public String getSelectedDate() {
    return selectedDate;
  }

  @Override
  public String toString() {
    return "DatePickerButtonAction{" +
      "selectedDate='" + selectedDate + '\'' +
      ", initialDate='" + initialDate + '\'' +
      '}';
  }
}
