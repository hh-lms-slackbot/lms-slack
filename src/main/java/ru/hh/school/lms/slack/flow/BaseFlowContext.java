package ru.hh.school.lms.slack.flow;

import com.github.seratch.jslack.api.model.block.LayoutBlock;

import java.util.ArrayList;
import java.util.List;

public class BaseFlowContext {

  private List<LayoutBlock> blocks;
  private ArrayList<String> callbackIds = new ArrayList<>();

  public List<LayoutBlock> getBlocks() {
    return this.blocks;
  }

  public void setBlocks(List<LayoutBlock> blocks) {
    this.blocks = blocks;
  }

  public List<String> getCallbackIds() {
    return callbackIds;
  }

  public void addCallbackId(String callbackId) {
    this.callbackIds.add(callbackId);
  }

}
