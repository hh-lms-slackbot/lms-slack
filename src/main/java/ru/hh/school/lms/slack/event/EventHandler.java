package ru.hh.school.lms.slack.event;

@FunctionalInterface
public interface EventHandler<T extends Event> {

  void onEvent(T event) throws EventHandlingException;
}
