package ru.hh.school.lms.slack.security;

import com.github.seratch.jslack.app_backend.SlackSignature;
import org.apache.commons.io.IOUtils;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Priority(Priorities.AUTHENTICATION)
public class SignatureFilter implements ContainerRequestFilter {

  private final SlackSignature.Generator generator;

  public SignatureFilter(Properties serviceProperties) {
    String secret = serviceProperties.getProperty("ru.hh.school.lms.slack.secret");
    this.generator = new SlackSignature.Generator(secret);
  }

  @Override
  public void filter(ContainerRequestContext request) throws IOException {
    if (!verifyRequest(request)) {
      abortWithUnauthorized(request);
    }
  }

  private boolean verifyRequest(ContainerRequestContext request) throws IOException {
    if (request != null && request.getHeaders() != null) {
      String requestTimestamp = request.getHeaderString(SlackSignature.HeaderNames.X_SLACK_REQUEST_TIMESTAMP);
      String requestBody = IOUtils.toString(request.getEntityStream(), StandardCharsets.UTF_8);
      InputStream in = IOUtils.toInputStream(requestBody, StandardCharsets.UTF_8);
      request.setEntityStream(in);
      String expected = this.generator.generate(requestTimestamp, requestBody);
      String actual = request.getHeaderString(SlackSignature.HeaderNames.X_SLACK_SIGNATURE);
      return actual != null && actual.equals(expected);
    } else {
      return false;
    }
  }

  private void abortWithUnauthorized(ContainerRequestContext request) {
    request.abortWith(
      Response
        .status(Response.Status.UNAUTHORIZED)
        .build()
    );
  }
}
