package ru.hh.school.lms.slack.blockaction;

import com.github.seratch.jslack.api.model.block.composition.TextObject;

public class SelectedOption {

  private TextObject text;
  private String value;

  public TextObject getText() {
    return text;
  }

  public String getValue() {
    return value;
  }
}
