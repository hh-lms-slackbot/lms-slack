package ru.hh.school.lms.slack.common;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.block.composition.OptionObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;

import java.util.List;

public class Attachments {
  public static Attachment createWarning(String text) {
    return Attachment.builder()
      .color(Colors.WARNING)
      .text(text)
      .mrkdwnIn(List.of("text"))
      .build();
  }

  public static OptionObject stringToOptionObject(String x) {
    return OptionObject.builder()
      .text(
        PlainTextObject.builder()
          .text(x)
          .build())
      .value(x)
      .build();
  }

}
