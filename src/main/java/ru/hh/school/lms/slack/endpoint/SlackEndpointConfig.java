package ru.hh.school.lms.slack.endpoint;

import com.github.seratch.jslack.api.model.block.ActionsBlock;
import com.github.seratch.jslack.api.model.block.ContextBlock;
import com.github.seratch.jslack.api.model.block.ContextBlockElement;
import com.github.seratch.jslack.api.model.block.DividerBlock;
import com.github.seratch.jslack.api.model.block.ImageBlock;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.OptionObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.api.model.block.composition.TextObject;
import com.github.seratch.jslack.api.model.block.element.BlockElement;
import com.github.seratch.jslack.api.model.block.element.ButtonElement;
import com.github.seratch.jslack.api.model.block.element.ChannelsSelectElement;
import com.github.seratch.jslack.api.model.block.element.ConversationsSelectElement;
import com.github.seratch.jslack.api.model.block.element.DatePickerElement;
import com.github.seratch.jslack.api.model.block.element.ExternalSelectElement;
import com.github.seratch.jslack.api.model.block.element.ImageElement;
import com.github.seratch.jslack.api.model.block.element.OverflowMenuElement;
import com.github.seratch.jslack.api.model.block.element.StaticSelectElement;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.hh.school.lms.slack.blockaction.UrlOptionObject;
import ru.hh.school.lms.slack.blockaction.BlockAction;
import ru.hh.school.lms.slack.blockaction.ButtonBlockAction;
import ru.hh.school.lms.slack.blockaction.DatePickerButtonAction;
import ru.hh.school.lms.slack.blockaction.OverflowBlockAction;
import ru.hh.school.lms.slack.blockaction.StaticSelectBlockAction;

import java.lang.reflect.Type;

@Configuration
class SlackEndpointConfig {

  @Bean
  Gson gson() {
    return new GsonBuilder()
      .registerTypeAdapter(LayoutBlock.class, new LayoutBlockDeserializer())
      .registerTypeAdapter(TextObject.class, new TextObjectDeserializer())
      .registerTypeAdapter(BlockElement.class, new BlockElementDeserializer())
      .registerTypeAdapter(ContextBlockElement.class, new ContextBlockElementDeserializer())
      .registerTypeAdapter(BlockAction.class, new BlockActionDeserializer())
      .registerTypeAdapter(OptionObject.class, new OptionObjectDeserializer())
      .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      .create();
  }

  static class TextObjectDeserializer implements JsonDeserializer<TextObject> {

    @Override
    public TextObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
      String type = json.getAsJsonObject().get("type").getAsString();
      switch (type) {
        case "plain_text":
          return context.deserialize(json, PlainTextObject.class);
        case "mrkdwn":
          return context.deserialize(json, MarkdownTextObject.class);
        default:
          return null;
      }
    }
  }


  static class ContextBlockElementDeserializer implements JsonDeserializer<ContextBlockElement> {

    @Override
    public ContextBlockElement deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
      String type = json.getAsJsonObject().get("type").getAsString();
      switch (type) {
        case "mrkdwn":
          return context.deserialize(json, MarkdownTextObject.class);
        case "plain_text":
          return context.deserialize(json, PlainTextObject.class);
        case "image":
          return context.deserialize(json, ImageElement.class);
        case "text":
          return context.deserialize(json, TextObject.class);
        default:
          return null;
      }
    }
  }


  static class BlockActionDeserializer implements JsonDeserializer<BlockAction> {

    @Override
    public BlockAction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
      String type = json.getAsJsonObject().get("type").getAsString();
      switch (type) {
        case "static_select":
          return context.deserialize(json, StaticSelectBlockAction.class);
        case "button":
          return context.deserialize(json, ButtonBlockAction.class);
        case "datepicker":
          return context.deserialize(json, DatePickerButtonAction.class);
        case "overflow":
          return context.deserialize(json, OverflowBlockAction.class);
        // case "":    - you can add your type if you need to process another type of element
        default:
          return null;
      }
    }
  }


  static class BlockElementDeserializer implements JsonDeserializer<BlockElement> {

    @Override
    public BlockElement deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
      String type = json.getAsJsonObject().get("type").getAsString();
      switch (type) {
        case "button":
          return context.deserialize(json, ButtonElement.class);
        case "datepicker":
          return context.deserialize(json, DatePickerElement.class);
        case "static_select":
          return context.deserialize(json, StaticSelectElement.class);
        case "context":
          return context.deserialize(json, ContextBlockElement.class);
        case "overflow":
          return context.deserialize(json, OverflowMenuElement.class);
        case "channels_select":
          return context.deserialize(json, ChannelsSelectElement.class);
        case "conversations_select":
          return context.deserialize(json, ConversationsSelectElement.class);
        case "external_select":
          return context.deserialize(json, ExternalSelectElement.class);
        case "image":
          return context.deserialize(json, ImageElement.class);
        default:
          return null;
      }
    }
  }


  static class LayoutBlockDeserializer implements JsonDeserializer<LayoutBlock> {
    @Override
    public LayoutBlock deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
      String type = json.getAsJsonObject().get("type").getAsString();
      switch (type) {
        case "section":
          return context.deserialize(json, SectionBlock.class);
        case "actions":
          return context.deserialize(json, ActionsBlock.class);
        case "context":
          return context.deserialize(json, ContextBlock.class);
        case "image":
          return context.deserialize(json, ImageBlock.class);
        case "divider":
          return context.deserialize(json, DividerBlock.class);
        default:
          return null;
      }
    }
  }

  static class OptionObjectDeserializer implements JsonDeserializer<OptionObject> {
    @Override
    public OptionObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
      return context.deserialize(json, UrlOptionObject.class);
    }
  }
}
