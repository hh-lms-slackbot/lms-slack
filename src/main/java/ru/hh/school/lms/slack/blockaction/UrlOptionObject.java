package ru.hh.school.lms.slack.blockaction;

import com.github.seratch.jslack.api.model.block.composition.OptionObject;

public class UrlOptionObject extends OptionObject {

  private String url;

  public UrlOptionObject(OptionObject source, String url) {
    super(source.getText(), source.getValue());
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
