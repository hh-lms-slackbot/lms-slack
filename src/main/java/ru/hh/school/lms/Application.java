package ru.hh.school.lms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.nab.starter.NabApplication;
import ru.hh.school.lms.slack.endpoint.ExceptionLogger;
import ru.hh.school.lms.slack.security.SignatureFeature;

public final class Application {

  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

  public static void main(String[] args) {
    LOGGER.info("Application is starting...");
    NabApplication.builder()
      .configureJersey(JerseyConfig.class)
      .registerResources(
        SignatureFeature.class,
        ExceptionLogger.class
      )
      .bindToRoot()
      .build()
      .run(ApplicationConfig.class);
  }
}
