package ru.hh.school.lms;

import org.springframework.context.annotation.Import;
import ru.hh.school.lms.demo.NotificationsResource;
import ru.hh.school.lms.demo.OnboardingResource;
import ru.hh.school.lms.slack.endpoint.SlackEndpoint;

/**
 * Register resource that handles POST request of
 * slash commands, interactive components and dialog webhooks.
 */

@Import({SlackEndpoint.class, NotificationsResource.class, OnboardingResource.class})
public class JerseyConfig {
}
