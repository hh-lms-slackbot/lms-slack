package ru.hh.school.lms.homework;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Conversation;
import com.github.seratch.jslack.api.model.User;
import com.github.seratch.jslack.api.model.block.ActionsBlock;
import com.github.seratch.jslack.api.model.block.DividerBlock;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.OptionGroupObject;
import com.github.seratch.jslack.api.model.block.composition.OptionObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.api.model.block.composition.TextObject;
import com.github.seratch.jslack.api.model.block.element.BlockElement;
import com.github.seratch.jslack.api.model.block.element.ButtonElement;
import com.github.seratch.jslack.api.model.block.element.DatePickerElement;
import com.github.seratch.jslack.api.model.block.element.StaticSelectElement;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogTextElement;
import com.github.seratch.jslack.app_backend.interactive_messages.response.ActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.sheets.SheetsClient;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.blockaction.DatePickerButtonAction;
import ru.hh.school.lms.slack.blockaction.StaticSelectBlockAction;
import ru.hh.school.lms.slack.common.Attachments;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;
import ru.hh.school.lms.slack.event.BlockActionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.flow.BaseFlow;
import ru.hh.school.lms.slack.interactionexception.Error;
import ru.hh.school.lms.slack.interactionexception.ErrorItem;
import ru.hh.school.lms.slack.interactionexception.InteractionException;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

public class HomeworkFlow extends BaseFlow {
  private static final Logger LOGGER = LoggerFactory.getLogger(HomeworkFlow.class);
  private static final int DEFAULT_SHEET = 0;
  private final String spreadsheetId;
  private final String studentChannelName;

  private final SlackHelper slack;
  private final MessageManager messageManager;
  private final SheetsClient sheetsClient;
  private final LectureDao lectureDao;
  private final HomeworkDao homeworkDao;
  private final Function<LocalDate, String> dateFormatter;

  private static final String BLOCK_ID_SELECT_DEADLINE = "select_schedule";
  private static final String BLOCK_ID_SELECT_LECTURE = "select_homeworks";
  private static final String BLOCK_ID_SELECT_LINK = "select_grades";
  private final String gradesSheetUrl;

  public HomeworkFlow(SlackHelper slack,
                      CallbackDispatcher dispatcher,
                      MessageManager messageManager,
                      SheetsClient sheetsClient,
                      Properties settings,
                      LectureDao lectureDao,
                      HomeworkDao homeworkDao,
                      Function<LocalDate, String> dateFormatter) {
    super(dispatcher);
    this.slack = slack;
    this.messageManager = messageManager;
    this.sheetsClient = sheetsClient;
    this.lectureDao = lectureDao;
    this.homeworkDao = homeworkDao;
    this.dateFormatter = dateFormatter;
    String sheetsUrlEditTemplate = settings.getProperty("sheets.url.template.edit");
    String gradesSheetName = messageManager.of("homeworks.sheet.name");
    Integer gid = sheetsClient.getSheetIdByTitle(gradesSheetName);
    this.spreadsheetId = settings.getProperty("ru.hh.school.lms.sheets.spreadsheetId");
    gradesSheetUrl = String.format(sheetsUrlEditTemplate, spreadsheetId, gid);
    this.studentChannelName = settings.getProperty("ru.hh.school.lms.role.student.channel");
  }

  public void show(BlockActionEvent event) {
    User lecturer = slack.getUser(event.getUser().getId())
      .orElseThrow(() -> new EventHandlingException("User from event was not found"));

    HomeworkFlowContext homework = new HomeworkFlowContext();

    List<String> lectures = lectureDao.getLecturesSorted().stream().map(Lecture::getTitle).collect(Collectors.toList());
    if (lectures.isEmpty()) {
      showWarning(event.getResponseUrl(), homework, messageManager.of("warning_list_of_lectures_is_empty"));
      return;
    }
    String lecturerEmail = lecturer.getProfile().getEmail();
    List<String> myLectures = lectureDao.getLecturesSorted().stream()
      .filter(lecture -> lecture.getLecturer().getEmail().equals(lecturerEmail))
      .map(Lecture::getTitle)
      .collect(Collectors.toList());

    homework.setDeadline(LocalDate.now());

    SectionBlock titleBlock = createSimpleSectionBlockWithText(messageManager.of("new_task_title"));

    List<OptionGroupObject> listOfLecturesOptionGroupObjects = new ArrayList<>();

    if (!myLectures.isEmpty()) {
      List<OptionObject> myLecturesOptions = myLectures.stream()
        .map(lecture ->
          OptionObject.builder()
            .text(PlainTextObject.builder()
              .text(lecture)
              .build())
            .value(String.valueOf(lectures.indexOf(lecture)))
            .build())
        .collect(Collectors.toList());

      OptionGroupObject myLecturesOptionGroup = OptionGroupObject.builder()
        .label(PlainTextObject.builder().text(messageManager.of("title_my_lectures")).build())
        .options(myLecturesOptions)
        .build();

      listOfLecturesOptionGroupObjects.add(myLecturesOptionGroup);
    }

    List<OptionObject> allLecturesOptions = lectures.stream()
      .map(lecture ->
        OptionObject.builder()
          .text(PlainTextObject.builder()
            .text(lecture)
            .build())
          .value(String.valueOf(lectures.indexOf(lecture)))
          .build())
      .collect(Collectors.toList());

    OptionGroupObject allLecturesOptionGroup = OptionGroupObject.builder()
      .label(PlainTextObject.builder().text(messageManager.of("title_all_lectures")).build())
      .options(allLecturesOptions)
      .build();

    listOfLecturesOptionGroupObjects.add(allLecturesOptionGroup);

    String selectLectureCallbackId = attachCallback("select_lecture", homework, this::selectLecture);

    SectionBlock lectureBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(messageManager.of("new_task_lecture"))
        .build())
      .accessory(StaticSelectElement.builder()
        .placeholder(PlainTextObject.builder()
          .text(messageManager.of("new_task_lecture_placeholder"))
          .build())
        .actionId(selectLectureCallbackId)
        .optionGroups(listOfLecturesOptionGroupObjects)
        .build())
      .blockId(BLOCK_ID_SELECT_LECTURE)
      .build();

    String selectDeadlineCallback = attachCallback("select_deadline", homework, this::selectDeadline);

    SectionBlock deadlineBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(messageManager.of("new_task_deadline"))
        .build())
      .accessory(DatePickerElement.builder()
        .initialDate(homework.getDeadlineStr())
        .placeholder(PlainTextObject.builder()
          .text(messageManager.of("new_task_deadline_placeholder"))
          .build())
        .actionId(selectDeadlineCallback)
        .build())
      .blockId(BLOCK_ID_SELECT_DEADLINE)
      .build();

    String addOrChangeLinkCallbackId = attachCallback("add_link", homework, this::addOrChangeLink);

    SectionBlock linkBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(messageManager.of("new_task_link_empty"))
        .build())
      .accessory(ButtonElement.builder()
        .text(PlainTextObject.builder()
          .text(messageManager.of("new_task_link_button"))
          .build())
        .actionId(addOrChangeLinkCallbackId)
        .build())
      .blockId(BLOCK_ID_SELECT_LINK)
      .build();

    DividerBlock divider = DividerBlock.builder().build();

    String submitNewTaskCallbackId = attachCallback("new_task_submit", homework, this::submitNewTask);

    String cancelNewTaskCallbackId = attachCallback("new_task_cancel", homework, this::cancelNewTask);

    List<BlockElement> buttonsBlocks = List.of(
      ButtonElement.builder()
        .text(PlainTextObject.builder()
          .emoji(true)
          .text(messageManager.of("new_task_submit"))
          .build())
        .actionId(submitNewTaskCallbackId)
        .build(),
      ButtonElement.builder()
        .text(PlainTextObject.builder()
          .emoji(true)
          .text(messageManager.of("new_task_cancel"))
          .build())
        .actionId(cancelNewTaskCallbackId)
        .build()
    );
    ActionsBlock buttons = ActionsBlock.builder()
      .elements(buttonsBlocks)
      .build();

    homework.setBlocks(
      List.of(
        titleBlock,
        lectureBlock,
        deadlineBlock,
        linkBlock,
        divider,
        buttons
      )
    );
    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(false)
        .blocks(homework.getBlocks())
        .build()
    );
  }

  private void selectDeadline(HomeworkFlowContext homework, BlockActionEvent event) {
    DatePickerButtonAction date = (DatePickerButtonAction) event.getActions().get(0);
    homework.setDeadlineStr(date.getSelectedDate());

    List<LayoutBlock> blocks = homework.getBlocks();

    SectionBlock deadlineBlock = (SectionBlock) blocks.stream()
      .filter(x -> sectionBlockElementContainsBlockId(x, BLOCK_ID_SELECT_DEADLINE))
      .findFirst().get();
    DatePickerElement datePickerElement = (DatePickerElement) deadlineBlock.getAccessory();
    datePickerElement.setInitialDate(homework.getDeadlineStr());

    List<Attachment> attachments = homework.isNewHomework() ?
      emptyList() :
      List.of(
        Attachments.createWarning(
          messageManager.of("warning_change_existing_task")
        )
      );

    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .blocks(blocks)
        .attachments(attachments)
        .build()
    );
  }

  private void selectLecture(HomeworkFlowContext homework, BlockActionEvent event) {
    List<Attachment> attachments = emptyList();
    StaticSelectBlockAction staticSelectBlockAction = (StaticSelectBlockAction) event.getActions().get(0);
    TextObject text = staticSelectBlockAction.getSelectedOption().getText();
    String value = staticSelectBlockAction.getSelectedOption().getValue();

    String lecture = ((PlainTextObject) text).getText();
    homework.setLecture(lecture);

    List<LayoutBlock> blocks = homework.getBlocks();

    SectionBlock lectureBlock = (SectionBlock) blocks.stream()
      .filter(x -> sectionBlockElementContainsBlockId(x, BLOCK_ID_SELECT_LECTURE))
      .findFirst().get();
    StaticSelectElement staticSelectElement = (StaticSelectElement) lectureBlock.getAccessory();
    staticSelectElement.setInitialOption(OptionObject.builder()
      .text(PlainTextObject.builder()
        .text(lecture)
        .build())
      .value(value)
      .build());

    Optional<Homework> selectedHomework = homeworkDao.getHomeworkByLectureTitle(lecture);
    SectionBlock titleBlock = (SectionBlock) blocks.get(0);
    SectionBlock deadlineBlock = (SectionBlock) blocks.stream()
      .filter(x -> sectionBlockElementContainsBlockId(x, BLOCK_ID_SELECT_DEADLINE))
      .findFirst().get();
    DatePickerElement deadlineDatePickerElement = (DatePickerElement) deadlineBlock.getAccessory();
    if (selectedHomework.isPresent()) {
      homework.setNewHomeworkFlag(false);
      homework.setDeadline(selectedHomework.get().getDeadline());
      homework.setLink(selectedHomework.get().getTaskLink());
      titleBlock.setText(MarkdownTextObject.builder()
        .text(messageManager.of("change_task_title"))
        .build());
      deadlineDatePickerElement.setInitialDate(selectedHomework.get().getDeadline().toString());
      changeLinkSectionBlock(blocks,
        messageManager.of("new_task_link", selectedHomework.get().getTaskLink()),
        messageManager.of("link_change_button"));
      attachments = List.of(Attachments.createWarning(messageManager.of("warning_change_existing_task")));
    } else if (!homework.isNewHomework()) {
      homework.setNewHomeworkFlag(true);
      homework.setDeadline(LocalDate.now());
      homework.setLink(null);
      titleBlock.setText(MarkdownTextObject.builder()
        .text(messageManager.of("new_task_title"))
        .build());
      deadlineDatePickerElement.setInitialDate(homework.getDeadlineStr());
      changeLinkSectionBlock(blocks,
        messageManager.of("new_task_link_empty"),
        messageManager.of("new_task_link_button"));
    }

    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .attachments(attachments)
        .blocks(blocks)
        .build()
    );
  }

  private void addOrChangeLink(HomeworkFlowContext homework, BlockActionEvent event) {
    String linkDialogSubmitId = attachCallback("link_dialog", homework,
      (HomeworkFlowContext h, DialogSubmissionEvent e) ->
        linkDialogSubmit(e, h, event.getResponseUrl()));

    Dialog dialog = Dialog.builder()
      .elements(List.of(
        DialogTextElement.builder()
          .minLength(4)
          .maxLength(150)
          .label(messageManager.of("link_dialog_label"))
          .placeholder(messageManager.of("link_dialog_placeholder"))
          .name(messageManager.of("link_dialog_name"))
          .value(homework.getLink())
          .build()))
      .title(messageManager.of("link_dialog_title"))
      .submitLabel(messageManager.of("link_save_button"))
      .callbackId(linkDialogSubmitId)
      .build();

    slack.dialog(event.getTriggerId(), dialog);
  }

  private void linkDialogSubmit(DialogSubmissionEvent event, HomeworkFlowContext homework, String responseUrl) {
    String link = event.getSubmission().get(messageManager.of("link_dialog_name"));
    try {
      URL url = new URL(link);
      url.toURI();
    } catch (MalformedURLException | URISyntaxException e) {
      Error error = new Error();
      error.setErrors(List.of(new ErrorItem(
        messageManager.of("link_dialog_name"),
        messageManager.of("entered_incorrect_link")
      )));
      throw new InteractionException("Entered incorrect link", error);
    }
    homework.setLink(link);

    List<LayoutBlock> blocks = homework.getBlocks();
    changeLinkSectionBlock(blocks,
      messageManager.of("new_task_link", link),
      messageManager.of("link_change_button"));

    List<Attachment> attachments = homework.isNewHomework() ?
      emptyList() :
      List.of(
        Attachments.createWarning(
          messageManager.of("warning_change_existing_task")
        )
      );

    slack.respond(
      responseUrl,
      ActionResponse.builder()
        .replaceOriginal(true)
        .blocks(blocks)
        .attachments(attachments)
        .build()
    );
  }

  private void changeLinkSectionBlock(List<LayoutBlock> blocks, String sectionText, String buttonText) {
    SectionBlock linkSectionBlock = (SectionBlock) blocks.stream()
      .filter(x -> sectionBlockElementContainsBlockId(x, BLOCK_ID_SELECT_LINK))
      .findFirst().get();
    linkSectionBlock.setText(MarkdownTextObject.builder()
      .text(sectionText)
      .build());
    ButtonElement buttonElement = (ButtonElement) linkSectionBlock.getAccessory();
    buttonElement.setText(PlainTextObject.builder()
      .text(buttonText)
      .build());
  }

  private void submitNewTask(HomeworkFlowContext homework, BlockActionEvent event) {
    if (homework.hasNullField()) {
      showWarning(
        event.getResponseUrl(),
        homework,
        messageManager.of("warning_some_field_required")
      );
      return;
    }

    SectionBlock titleBlockForLecturer;
    SectionBlock titleBlockForNotification;
    if (homeworkDao.getHomeworkByLectureTitle(homework.getLecture()).isPresent()) {
      titleBlockForLecturer = createSimpleSectionBlockWithText(
        messageManager.of("new_task_notification_changed"));
      titleBlockForNotification = createSimpleSectionBlockWithText(
        messageManager.of("new_task_notification_title_changed", event.getUser().getId()));
    } else {
      titleBlockForLecturer = createSimpleSectionBlockWithText(
        messageManager.of("new_task_notification_created"));
      titleBlockForNotification = createSimpleSectionBlockWithText(
        messageManager.of("new_task_notification_title", event.getUser().getId()));
    }

    DividerBlock dividerBlock = DividerBlock.builder().build();

    SectionBlock dataBlock = SectionBlock.builder()
      .fields(List.of(
        MarkdownTextObject.builder()
          .text(messageManager.of("new_task_notification_deadline"))
          .build(),
        MarkdownTextObject.builder()
          .text(dateFormatter.apply(homework.getDeadline()))
          .build(),
        MarkdownTextObject.builder()
          .text(messageManager.of("new_task_notification_lecture"))
          .build(),
        MarkdownTextObject.builder()
          .text(homework.getLecture())
          .build(),
        MarkdownTextObject.builder()
          .text(messageManager.of("new_task_notification_link"))
          .build(),
        MarkdownTextObject.builder()
          .text(messageManager.of("new_task_notification_link_format", homework.getLink()))
          .build(),
        MarkdownTextObject.builder()
          .text(String.format("<%s|%s>", gradesSheetUrl, messageManager.of("all_tasks_link")))
          .build()
      ))
      .build();

    try {
      homeworkDao.saveHomework(homework.getLecture(), homework.getDeadline(), homework.getLink());
    } catch (LmsValueNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      showWarning(
        event.getResponseUrl(),
        homework,
        messageManager.of("warning_no_lecture")
      );
      return;
    }

    Optional<Conversation> studentChannel = slack.getConversationByName(studentChannelName);
    studentChannel.ifPresent(conversation -> slack.post(conversation.getId(), List.of(titleBlockForNotification, dividerBlock, dataBlock)));

    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .blocks(
          List.of(
            titleBlockForLecturer,
            dividerBlock,
            dataBlock
          )
        )
        .build()
    );

    dropContext(homework);
  }

  private void cancelNewTask(HomeworkFlowContext homework, BlockActionEvent event) {
    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .text(messageManager.of("new_task_notification_canceled"))
        .build()
    );

    dropContext(homework);
  }

  private void showWarning(String responseUrl, HomeworkFlowContext context, String msg) {
    String warning = messageManager.of("warning_message", msg);
    slack.respond(
      responseUrl,
      ActionResponse.builder()
        .replaceOriginal(true)
        .attachments(List.of(Attachments.createWarning(warning)))
        .blocks(context.getBlocks())
        .build()
    );
  }

  private boolean sectionBlockElementContainsBlockId(LayoutBlock block, String blockId) {
    if (block instanceof SectionBlock) {
      SectionBlock sectionBlock = (SectionBlock) block;
      return Objects.equals(sectionBlock.getBlockId(), blockId);
    }
    return false;
  }

  private SectionBlock createSimpleSectionBlockWithText(String text) {
    return SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(text)
        .build())
      .build();
  }
}
