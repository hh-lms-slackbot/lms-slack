package ru.hh.school.lms.homework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.sheets.SheetsClient;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Properties;
import java.util.function.Function;

@Configuration
public class HomeworkConfig {

  @Autowired
  private Locale locale;

  @Bean(name = "homeworkMessage")
  public MessageManager message() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("homework/labels", "sheets/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return (messageCode, args) -> messageSource.getMessage(messageCode, args, locale);
  }

  @Bean
  public HomeworkFlow homeworkFlow(
    SlackHelper slack,
    CallbackDispatcher dispatcher,
    @Qualifier("homeworkMessage") MessageManager messageManager,
    SheetsClient sheetsClient,
    @Qualifier("serviceProperties") Properties settings,
    LectureDao lectureDao,
    HomeworkDao homeworkDao,
    @Qualifier("dayOfWeek") Function<LocalDate, String> dateFormatter
  ) {
    return new HomeworkFlow(
      slack,
      dispatcher,
      messageManager,
      sheetsClient,
      settings,
      lectureDao,
      homeworkDao,
      dateFormatter);
  }
}
