package ru.hh.school.lms.homework;

import ru.hh.school.lms.slack.flow.BaseFlowContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class HomeworkFlowContext extends BaseFlowContext {

  private LocalDate deadline;
  private String lecture;
  private String link;
  private boolean newHomeworkFlag = true;

  LocalDate getDeadline() {
    return deadline;
  }

  String getDeadlineStr() {
    return deadline.format(DateTimeFormatter.ISO_DATE);
  }

  void setDeadline(LocalDate deadline) {
    this.deadline = deadline;
  }

  void setDeadlineStr(String deadline) {
    this.deadline = LocalDate.parse(deadline, DateTimeFormatter.ISO_DATE);
  }

  String getLecture() {
    return lecture;
  }

  void setLecture(String lecture) {
    this.lecture = lecture;
  }

  String getLink() {
    return link;
  }

  void setLink(String link) {
    this.link = link;
  }

  boolean isNewHomework() {
    return newHomeworkFlag;
  }

  void setNewHomeworkFlag(boolean newHomeworkFlag) {
    this.newHomeworkFlag = newHomeworkFlag;
  }

  boolean hasNullField() {
    return deadline == null || lecture == null || link == null;
  }
}
