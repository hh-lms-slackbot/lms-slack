package ru.hh.school.lms.persistence;

import ru.hh.school.lms.persistence.entity.Homework;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface HomeworkDao {
  void saveHomework(String lectureTitle, LocalDate deadline, String taskLink) throws LmsValueNotFoundException;

  Optional<Homework> getHomeworkByLectureTitle(String lectureTitle);

  List<Homework> getHomeworksSortedByDeadline();

  void saveHomework(Homework homework) throws LmsValueNotFoundException;
}
