package ru.hh.school.lms.persistence.sheets.impl;

import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.cache.HomeworkCacheDao;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.sheets.GoogleSheetsSynchronizer;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class HomeworkSheetsDao implements HomeworkDao {
  private final HomeworkCacheDao homeworkCacheDao;
  private final GoogleSheetsSynchronizer synchronizer;

  public HomeworkSheetsDao(HomeworkCacheDao homeworkCacheDao, GoogleSheetsSynchronizer synchronizer) {
    this.homeworkCacheDao = homeworkCacheDao;
    this.synchronizer = synchronizer;
  }

  @Override
  public void saveHomework(String lectureTitle, LocalDate deadline, String taskLink) throws LmsValueNotFoundException {
    homeworkCacheDao.saveHomework(lectureTitle, deadline, taskLink);
    synchronizer.cacheChanged();
  }

  @Override
  public Optional<Homework> getHomeworkByLectureTitle(String lectureTitle) {
    return homeworkCacheDao.getHomeworkByLectureTitle(lectureTitle);
  }

  @Override
  public List<Homework> getHomeworksSortedByDeadline() {
    return homeworkCacheDao.getHomeworksSortedByDeadline();
  }

  @Override
  public void saveHomework(Homework homework) throws LmsValueNotFoundException {
    homeworkCacheDao.saveHomework(homework);
    synchronizer.cacheChanged();
  }
}
