package ru.hh.school.lms.persistence.entity;

import java.util.Objects;

public class Grade {
  final private Person student;
  final private Homework homework;
  final private Integer value;

  public Grade(Person student, Homework homework, Integer value) {
    this.student = student;
    this.homework = homework;
    this.value = value;
  }

  public Person getStudent() {
    return student;
  }

  public Homework getHomework() {
    return homework;
  }

  public int getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Grade)) {
      return false;
    }
    Grade grade = (Grade) o;
    return Objects.equals(student, grade.student) &&
      Objects.equals(homework, grade.homework);
  }

  @Override
  public int hashCode() {
    return Objects.hash(student, homework);
  }

  @Override
  public String toString() {
    return "Grade{" +
      "student=" + student +
      ", homework=" + homework +
      ", value=" + value +
      '}';
  }
}
