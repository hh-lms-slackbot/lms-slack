package ru.hh.school.lms.persistence.sheets;

import com.google.api.services.sheets.v4.model.ValueRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import ru.hh.school.lms.persistence.cache.CacheDaoManager;
import ru.hh.school.lms.persistence.sheets.relation.GoogleSheetRelation;
import ru.hh.school.lms.persistence.sheets.relation.MapSheetToCacheException;
import ru.hh.school.lms.sheets.SheetsClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GoogleSheetsSynchronizer {

  private static final Logger LOGGER = LoggerFactory.getLogger(GoogleSheetsSynchronizer.class);

  private final SheetsClient sheetsClient;
  private final List<GoogleSheetRelation> relations;
  private final AnnotationConfigWebApplicationContext appContext;
  private List<List<List<Object>>> prevValues;

  private boolean cacheChanged;

  private final CacheDaoManager cacheDaoManager;

  @Autowired
  public GoogleSheetsSynchronizer(SheetsClient sheetsClient,
                                  @Qualifier("orderedRelations") List<GoogleSheetRelation> relations,
                                  AnnotationConfigWebApplicationContext appContext,
                                  CacheDaoManager cacheDaoManager) {
    this.sheetsClient = sheetsClient;
    this.relations = relations;
    this.appContext = appContext;
    this.cacheDaoManager = cacheDaoManager;
  }

  @EventListener
  private void initialSynchronization(ContextRefreshedEvent event) throws IOException, MapSheetToCacheException {
    final ApplicationContext applicationContext = event.getApplicationContext();
    if (!appContext.equals(applicationContext)) {
      return;
    }
    boolean sheetsAreValid = flushSheetsToCache();

    if (sheetsAreValid) {
      sheetsClient.executeBatchUpdate(
        relations.stream()
          .map(r -> r.initialRequestsToExecute(sheetsClient))
          .flatMap(List::stream)
          .collect(Collectors.toList())
      );
      flushCacheToSheets();
    } else {
      throw new MapSheetToCacheException("Invalid sheets, cannot start application.");
    }
  }

  @Scheduled(fixedRate = 5_000, initialDelay = 15_000)
  private void synchronize() throws IOException {
    if (cacheChanged) {
      LOGGER.info("Cache changed. Start sheets update.");
      flushCacheToSheets();
      cacheChanged = false;
      return;
    }
    flushSheetsToCache();
  }

  private boolean flushSheetsToCache() throws IOException {
    boolean success = true;
    // read data from sheets
    List<ValueRange> dataFromSheets = sheetsClient.readValueRanges(
      relations.stream().map(GoogleSheetRelation::getSheetTitle).collect(Collectors.toList()) // list of ranges (sheet name is also range)
    );
    // if data from sheets changed => update cache with it
    if (changed(dataFromSheets)) {
      try {
        LOGGER.info("Some changes in sheets. Start cache update.");
        for (GoogleSheetRelation relation : relations) {
          relation.mapSheetToCache(relation.getCorrespondingValueRange(dataFromSheets), cacheDaoManager);
        }
        cacheDaoManager.saveTemporaryCache(); // if success => override cache
      } catch (MapSheetToCacheException e) {
        LOGGER.error(e.getMessage(), e);
        success = false;
      }
    }
    return success;
  }

  // TODO: 18.05.2019 add google docs hooks, to avoid this crutch
  private boolean changed(List<ValueRange> fromSheets) {
    List<List<List<Object>>> newValesCopy = fromSheets.stream().map(ValueRange::getValues)
      .map(values -> values.stream().map(List::copyOf).collect(Collectors.toList()))
      .collect(Collectors.toList());
    if (!newValesCopy.equals(prevValues)) {
      prevValues = newValesCopy;
      return true;
    }
    return false;
  }

  public void cacheChanged() {
    cacheChanged = true;
  }

  private void flushCacheToSheets() {
    List<ValueRange> dataToFlush = new ArrayList<>();
    try {
      for (GoogleSheetRelation relation : relations) {
        dataToFlush.add(relation.emptyRangeForClear());
        dataToFlush.add(relation.mapCacheToSheet(cacheDaoManager));
      }
      sheetsClient.batchUpdateValues(dataToFlush);
    } catch (IOException exc) {
      LOGGER.error("Google Sheets synchronization failed", exc);
    }
  }
}
