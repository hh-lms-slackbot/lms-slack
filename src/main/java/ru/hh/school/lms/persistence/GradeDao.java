package ru.hh.school.lms.persistence;

import ru.hh.school.lms.persistence.entity.Grade;

import java.util.List;
import java.util.Optional;

public interface GradeDao {

  void saveGrade(String studentEmail, String lectureTitleOfHw, Integer gradeValue) throws LmsValueNotFoundException;

  List<Grade> getStudentGrades(String studentEmail);

  Optional<Grade> getGradeByStudentByHomework(String studentEmail, String hwLectureTitle);
}
