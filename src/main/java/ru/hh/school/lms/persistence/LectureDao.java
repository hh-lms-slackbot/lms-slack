package ru.hh.school.lms.persistence;

import ru.hh.school.lms.persistence.entity.Lecture;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface LectureDao {
  Optional<Lecture> getLectureByTitle(String title);

  List<Lecture> getLecturesSorted();

  void saveLecture(String lectureTitle, LocalDate date, String lecturerEmail, String specialization, boolean homework)
    throws LmsValueNotFoundException;

  void saveLecture(Lecture lectureToSave) throws LmsValueNotFoundException;
}
