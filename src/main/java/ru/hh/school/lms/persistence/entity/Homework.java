package ru.hh.school.lms.persistence.entity;

import java.time.LocalDate;
import java.util.Objects;

public class Homework implements Comparable<Homework> {
  // id (unique)
  final private Lecture lecture;
  final private LocalDate deadline;
  final private String taskLink;

  public Homework(Lecture lecture, LocalDate deadline, String taskLink) {
    this.lecture = lecture;
    this.deadline = deadline;
    this.taskLink = taskLink;
  }

  public Lecture getLecture() {
    return lecture;
  }

  public LocalDate getDeadline() {
    return deadline;
  }

  public String getTaskLink() {
    return taskLink;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Homework)) {
      return false;
    }
    Homework homework = (Homework) o;
    return Objects.equals(lecture, homework.lecture);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lecture);
  }

  @Override
  public String toString() {
    return "HomeworkContext{" +
      "lecture=" + lecture +
      ", deadline=" + deadline +
      ", taskLink='" + taskLink + '\'' +
      '}';
  }

  @Override
  public int compareTo(Homework o) {
    return deadline.compareTo(o.deadline);
  }
}
