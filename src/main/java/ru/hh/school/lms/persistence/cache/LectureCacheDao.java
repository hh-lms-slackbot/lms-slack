package ru.hh.school.lms.persistence.cache;

import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.persistence.entity.Person;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class LectureCacheDao implements LectureDao {
  private final Map<String, Lecture> lecturesMap;
  private PersonCacheDao personCacheDao;

  public LectureCacheDao(PersonCacheDao personCacheDao, DataSource cache) {
    this.personCacheDao = personCacheDao;
    lecturesMap = cache.getLecturesMap();
  }

  @Override
  public Optional<Lecture> getLectureByTitle(String title) {
    if (lecturesMap.containsKey(title)) {
      return Optional.of(lecturesMap.get(title));
    }
    return Optional.empty();
  }

  @Override
  public List<Lecture> getLecturesSorted() {
    return lecturesMap.values().stream().sorted().collect(Collectors.toList());
  }

  @Override
  public void saveLecture(String lectureTitle, LocalDate date, String lecturerEmail, String specialization, boolean homework)
    throws LmsValueNotFoundException {
    saveLecture(new Lecture(lectureTitle, date, getLecturerByEmailIfExist(lecturerEmail), specialization, homework));
  }

  @Override
  public void saveLecture(Lecture lectureToSave) throws LmsValueNotFoundException {
    getLecturerByEmailIfExist(lectureToSave.getLecturer().getEmail());
    lecturesMap.put(lectureToSave.getTitle(), lectureToSave);
  }

  private Person getLecturerByEmailIfExist(String lecturerEmail) throws LmsValueNotFoundException {
    Optional<Person> lecturerOpt = personCacheDao.getPersonByEmail(lecturerEmail);
    return lecturerOpt.orElseThrow(() -> new LmsValueNotFoundException("No lecturer found with email: " + lecturerEmail));
  }

  public void deleteAll() {
    lecturesMap.clear();
  }
}
