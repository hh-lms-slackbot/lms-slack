package ru.hh.school.lms.persistence.cache;

import ru.hh.school.lms.persistence.entity.Grade;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.persistence.entity.Person;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class DataSource {
  private Map<Person, Map<Homework, Grade>> gradesMap = new ConcurrentHashMap<>();
  private Map<Lecture, Homework> homeworksMap = new ConcurrentHashMap<>();
  private Map<String, Lecture> lecturesMap = new ConcurrentHashMap<>();
  private Map<String, Person> personsMap = new ConcurrentHashMap<>();

  void replaceWith(DataSource tempCache) {
    clear();
    gradesMap.putAll(tempCache.getGradesMap());
    homeworksMap.putAll(tempCache.getHomeworksMap());
    lecturesMap.putAll(tempCache.getLecturesMap());
    personsMap.putAll(tempCache.getPersonsMap());
  }

  void clear() {
    gradesMap.clear();
    homeworksMap.clear();
    lecturesMap.clear();
    personsMap.clear();
  }

  Map<Person, Map<Homework, Grade>> getGradesMap() {
    return gradesMap;
  }

  Map<Lecture, Homework> getHomeworksMap() {
    return homeworksMap;
  }

  Map<String, Lecture> getLecturesMap() {
    return lecturesMap;
  }

  Map<String, Person> getPersonsMap() {
    return personsMap;
  }
}
