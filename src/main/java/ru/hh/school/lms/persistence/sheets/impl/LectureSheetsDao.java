package ru.hh.school.lms.persistence.sheets.impl;

import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.cache.LectureCacheDao;
import ru.hh.school.lms.persistence.entity.Lecture;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class LectureSheetsDao implements LectureDao {
  private final LectureCacheDao lectureCacheDao;

  public LectureSheetsDao(LectureCacheDao lectureCacheDao) {
    this.lectureCacheDao = lectureCacheDao;
  }

  @Override
  public Optional<Lecture> getLectureByTitle(String title) {
    return lectureCacheDao.getLectureByTitle(title);
  }

  @Override
  public List<Lecture> getLecturesSorted() {
    return lectureCacheDao.getLecturesSorted();
  }

  @Override
  public void saveLecture(String lectureTitle, LocalDate date, String lecturerEmail, String specialization, boolean homework)
    throws LmsValueNotFoundException {
    lectureCacheDao.saveLecture(lectureTitle, date, lecturerEmail, specialization, homework);
  }

  @Override
  public void saveLecture(Lecture lectureToSave) throws LmsValueNotFoundException {
    lectureCacheDao.saveLecture(lectureToSave);
  }
}
