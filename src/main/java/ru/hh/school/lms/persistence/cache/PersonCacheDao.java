package ru.hh.school.lms.persistence.cache;

import ru.hh.school.lms.persistence.PersonDao;
import ru.hh.school.lms.persistence.entity.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PersonCacheDao implements PersonDao {
  private final Map<String, Person> personsMap;

  public PersonCacheDao(DataSource cache) {
    this.personsMap = cache.getPersonsMap();
  }

  @Override
  public Optional<Person> getPersonByEmail(String email) {
    return Optional.ofNullable(personsMap.get(email));
  }

  @Override
  public List<Person> getAllPersons() {
    return new ArrayList<>(personsMap.values());
  }

  @Override
  public void savePerson(Person newPerson) {
    personsMap.put(newPerson.getEmail(), newPerson);
  }

  @Override
  public List<Person> getLecturers() {
    return filteredPersons(Person::isLecturer);
  }

  @Override
  public List<Person> getAdmins() {
    return filteredPersons(Person::isAdmin);
  }

  @Override
  public List<Person> getStudents() {
    return filteredPersons(Person::isStudent);
  }

  private List<Person> filteredPersons(Predicate<Person> condition) {
    return personsMap.values().stream().filter(condition).sorted().collect(Collectors.toList());
  }

  public void deleteAll() {
    personsMap.clear();
  }
}
