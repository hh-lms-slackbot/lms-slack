package ru.hh.school.lms.persistence;

import ru.hh.school.lms.persistence.entity.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDao {
  Optional<Person> getPersonByEmail(String email);

  List<Person> getAllPersons();

  void savePerson(Person newPerson);

  List<Person> getLecturers();

  List<Person> getAdmins();

  List<Person> getStudents();
}
