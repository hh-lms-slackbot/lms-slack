package ru.hh.school.lms.persistence.sheets.relation;

public class MapSheetToCacheException extends Exception {
  public MapSheetToCacheException() {
  }

  public MapSheetToCacheException(String message) {
    super(message);
  }

  public MapSheetToCacheException(String message, Throwable cause) {
    super(message, cause);
  }

  public MapSheetToCacheException(Throwable cause) {
    super(cause);
  }
}
