package ru.hh.school.lms.persistence.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Lecture;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class HomeworkCacheDao implements HomeworkDao {

  private static final Logger LOGGER = LoggerFactory.getLogger(HomeworkCacheDao.class);

  private final Map<Lecture, Homework> homeworksMap;

  private LectureCacheDao lectureCacheDao;

  public HomeworkCacheDao(LectureCacheDao lectureCacheDao, DataSource cache) {
    this.lectureCacheDao = lectureCacheDao;
    homeworksMap = cache.getHomeworksMap();
  }

  @Override
  public void saveHomework(String lectureTitle, LocalDate deadline, String taskLink) throws LmsValueNotFoundException {
    saveHomework(new Homework(getLectureByTitleIfExist(lectureTitle), deadline, taskLink));
  }

  @Override
  public Optional<Homework> getHomeworkByLectureTitle(String lectureTitle) {
    for (Homework hw : homeworksMap.values()) {
      if (lectureTitle.equals(hw.getLecture().getTitle())) {
        return Optional.of(hw);
      }
    }
    return Optional.empty();
  }

  @Override
  public List<Homework> getHomeworksSortedByDeadline() {
    return homeworksMap.values().stream().sorted().collect(Collectors.toList());
  }

  @Override
  public void saveHomework(Homework homework) throws LmsValueNotFoundException {
    getLectureByTitleIfExist(homework.getLecture().getTitle()); //check if homework with title exist
    homeworksMap.put(homework.getLecture(), homework);
  }

  private Lecture getLectureByTitleIfExist(String lectureTitle) throws LmsValueNotFoundException {
    Optional<Lecture> lectureOpt = lectureCacheDao.getLectureByTitle(lectureTitle);
    return lectureOpt.orElseThrow(() -> new LmsValueNotFoundException("No lecture found with title: " + lectureTitle));
  }

  public void deleteAll() {
    homeworksMap.clear();
  }
}
