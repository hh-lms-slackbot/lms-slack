package ru.hh.school.lms.persistence.sheets.relation;

import com.google.api.services.sheets.v4.model.ConditionValue;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.ValueRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.cache.CacheDaoManager;
import ru.hh.school.lms.persistence.cache.HomeworkCacheDao;
import ru.hh.school.lms.persistence.cache.LectureCacheDao;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.sheets.SheetsClient;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
@PropertySource(value = "classpath:sheets/labels.properties", encoding = "UTF-8")
public class HomeworksSheetRelation implements GoogleSheetRelation {
  private Function<String, LocalDate> dateParser;

  @Value("#{'${homeworks.sheet.headers}'.split(',')}")
  private List<Object> headers;

  @Value("${homeworks.sheet.name}")
  private String sheetName;

  @Value("${lectures.sheet.titlesRange}")
  private String lecturesSheetTitlesRange;

  private static final int LECTURE_TITLE_INDEX = 0;
  private static final int DEADLINE_INDEX = 1;
  private static final int HW_LINK_INDEX = 2;

  @Autowired
  public HomeworksSheetRelation(CacheDaoManager cacheDaoManager, Function<String, LocalDate> dateParser) {
    this.dateParser = dateParser;
  }

  @Override
  public void mapSheetToCache(ValueRange valueRange, CacheDaoManager cacheDaoManager) throws MapSheetToCacheException {
    HomeworkCacheDao homeworkCacheTempDao = cacheDaoManager.getHwTempDao();
    LectureCacheDao lectureCacheTempDao = cacheDaoManager.getLectureTempDao();

    homeworkCacheTempDao.deleteAll();
    List<List<Object>> values = valueRange.getValues();
    values.remove(0); // remove headers
    for (List<Object> row : values) {
      if (row.isEmpty()) { // ignore empty rows
        continue;
      }
      try {
        homeworkCacheTempDao.saveHomework(
          mapRowToHomework(row, lectureCacheTempDao)
        );
      } catch (LmsValueNotFoundException e) {
        throw new MapSheetToCacheException(e);
      }
    }
  }

  private Homework mapRowToHomework(List<Object> row, LectureCacheDao lectureCacheTempDao) throws MapSheetToCacheException {
    int rowSize = row.size();
    for (int i = 0; i < headers.size() - rowSize; i++) {
      row.add("");
    }
    Optional<Lecture> lectureOfHwOpt = lectureCacheTempDao.getLectureByTitle(row.get(LECTURE_TITLE_INDEX).toString());
    if (lectureOfHwOpt.isEmpty()) {
      throw new MapSheetToCacheException("No lecture with title: '" + row.get(LECTURE_TITLE_INDEX) + "'");
    }
    return new Homework(
      lectureOfHwOpt.get(),
      dateParser.apply(row.get(DEADLINE_INDEX).toString()),
      row.get(HW_LINK_INDEX).toString()
    );
  }

  @Override
  public ValueRange mapCacheToSheet(CacheDaoManager cacheDaoManager) {
    HomeworkCacheDao homeworkCacheDao = cacheDaoManager.getHwDao();
    LectureCacheDao lectureCacheDao = cacheDaoManager.getLectureDao();

    List<List<Object>> valuesToFlush = new ArrayList<>(Collections.singletonList(headers));
    for (Homework hw : homeworkCacheDao.getHomeworksSortedByDeadline()) {
      List<Object> row = new ArrayList<>(headers); // init with some values
      row.set(LECTURE_TITLE_INDEX, linkToLectureTitleCell(hw.getLecture(), lectureCacheDao));
      row.set(DEADLINE_INDEX, hw.getDeadline().toString());
      row.set(HW_LINK_INDEX, hw.getTaskLink());
      valuesToFlush.add(row);
    }
    return new ValueRange().setValues(valuesToFlush).setRange(getSheetTitle());
  }

  private Object linkToLectureTitleCell(Lecture lecture, LectureCacheDao lectureCacheDao) {
    int indexOffset = 2; // headers + sheet first index is 1

    int rowIndex = lectureCacheDao.getLecturesSorted().indexOf(lecture) + indexOffset;
    // from range (example: =Лекции!B2:B) last char is 'B' - column letter, '=Лекции' - sheet title
    String columnLetter = lecturesSheetTitlesRange.substring(lecturesSheetTitlesRange.length() - 1);
    String sheetTitle = lecturesSheetTitlesRange.split("!")[0];
    return String.format("%s!%s%d", sheetTitle, columnLetter, rowIndex);
  }

  @Override
  public List<Request> initialRequestsToExecute(SheetsClient client) {
    int sheetId = client.getSheetIdByTitle(getSheetTitle());
    return List.of(
      client.dateRenderRequest(DEADLINE_INDEX, sheetId),
      client.dateValidationRequest(DEADLINE_INDEX, sheetId),
      client.dataValidationRequest(
        client.getColumnGridRange(LECTURE_TITLE_INDEX, sheetId),
        "ONE_OF_RANGE",
        List.of(new ConditionValue().setUserEnteredValue(lecturesSheetTitlesRange))),
      client.dataValidationRequest(
        client.getColumnGridRange(HW_LINK_INDEX, sheetId),
        "TEXT_IS_URL",
        Collections.emptyList()
      )
    );
  }

  @Override
  public String getSheetTitle() {
    return sheetName;
  }
}
