package ru.hh.school.lms.persistence.sheets.impl;

import ru.hh.school.lms.persistence.PersonDao;
import ru.hh.school.lms.persistence.cache.PersonCacheDao;
import ru.hh.school.lms.persistence.entity.Person;

import java.util.List;
import java.util.Optional;

public class PersonSheetsDao implements PersonDao {
  private final PersonCacheDao personCacheDao;

  public PersonSheetsDao(PersonCacheDao personCacheDao) {
    this.personCacheDao = personCacheDao;
  }

  @Override
  public Optional<Person> getPersonByEmail(String email) {
    return personCacheDao.getPersonByEmail(email);
  }

  @Override
  public List<Person> getAllPersons() {
    return personCacheDao.getAllPersons();
  }

  @Override
  public void savePerson(Person newPerson) {
    personCacheDao.savePerson(newPerson);
    // send to sheets?
  }

  @Override
  public List<Person> getLecturers() {
    return personCacheDao.getLecturers();
  }

  @Override
  public List<Person> getAdmins() {
    return personCacheDao.getAdmins();
  }

  @Override
  public List<Person> getStudents() {
    return personCacheDao.getStudents();
  }
}
