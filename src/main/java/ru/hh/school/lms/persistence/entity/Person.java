package ru.hh.school.lms.persistence.entity;

import java.util.Objects;

public class Person implements Comparable<Person> {
  // id (unique)
  final private String email;
  final private String fullName;
  final private boolean student;
  final private boolean lecturer;
  final private boolean admin;

  public Person(String email, String fullName, boolean student, boolean lecturer, boolean admin) {
    this.email = email;
    this.fullName = fullName;
    this.student = student;
    this.lecturer = lecturer;
    this.admin = admin;
  }

  public String getEmail() {
    return email;
  }

  public String getFullName() {
    return fullName;
  }

  public boolean isStudent() {
    return student;
  }

  public boolean isLecturer() {
    return lecturer;
  }

  public boolean isAdmin() {
    return admin;
  }

  @Override
  public String toString() {
    return "Person{" +
      "email='" + email + '\'' +
      ", fullName='" + fullName + '\'' +
      ", Roles={" + (admin ? "A" : "") + (lecturer ? "L" : "") + (student ? "S" : "") + "}" +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Person)) {
      return false;
    }
    Person person = (Person) o;
    return email.equals(person.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email);
  }

  @Override
  public int compareTo(Person o) {
    return fullName.compareTo(o.fullName);
  }
}
