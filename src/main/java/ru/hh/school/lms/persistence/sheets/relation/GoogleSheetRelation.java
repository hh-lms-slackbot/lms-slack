package ru.hh.school.lms.persistence.sheets.relation;

import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.ValueRange;
import ru.hh.school.lms.persistence.cache.CacheDaoManager;
import ru.hh.school.lms.sheets.SheetsClient;

import java.util.Collections;
import java.util.List;

public interface GoogleSheetRelation {

  void mapSheetToCache(ValueRange valueRange, CacheDaoManager cacheDaoManager) throws MapSheetToCacheException;

  ValueRange mapCacheToSheet(CacheDaoManager cacheDaoManager);

  default ValueRange emptyRangeForClear() {
    List<List<Object>> clearValues = Collections.nCopies(100, Collections.nCopies(100, ""));
    return new ValueRange().setValues(clearValues).setRange(getSheetTitle());
  }

  String getSheetTitle();

  default List<Request> initialRequestsToExecute(SheetsClient client) {
    return Collections.emptyList();
  }

  default ValueRange getCorrespondingValueRange(List<ValueRange> valueRanges) {
    for (ValueRange vr : valueRanges) {
      String rangeRow = vr.getRange();
      // value range format: "'sheet_name'!A1:Z1000"
      String sheetName = rangeRow.split("!")[0].replaceAll("'", "");
      if (getSheetTitle().equals(sheetName)) {
        return vr;
      }
    }
    return null;
  }
}
