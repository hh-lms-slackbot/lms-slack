package ru.hh.school.lms.persistence.sheets.relation;

import com.google.api.services.sheets.v4.model.ConditionValue;
import com.google.api.services.sheets.v4.model.GridRange;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.ValueRange;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.cache.CacheDaoManager;
import ru.hh.school.lms.persistence.cache.GradeCacheDao;
import ru.hh.school.lms.persistence.cache.HomeworkCacheDao;
import ru.hh.school.lms.persistence.cache.PersonCacheDao;
import ru.hh.school.lms.persistence.entity.Grade;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Person;
import ru.hh.school.lms.sheets.SheetsClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@PropertySource(value = "classpath:sheets/labels.properties", encoding = "UTF-8")
public class GradesSheetRelation implements GoogleSheetRelation {
  @Value("#{'${grades.sheet.headers}'.split(',')}")
  private List<Object> headers;

  @Value("${grades.sheet.name}")
  private String sheetName;

  @Value("${homeworks.sheet.titlesRange}")
  private String homeworksSheetTitlesRange;

  @Override
  public void mapSheetToCache(ValueRange valueRange, CacheDaoManager cacheDaoManager) throws MapSheetToCacheException {
    PersonCacheDao personCacheTempDao = cacheDaoManager.getPersonTempDao();
    HomeworkCacheDao homeworkCacheTempDao = cacheDaoManager.getHwTempDao();
    GradeCacheDao gradeCacheTempDao = cacheDaoManager.getGradeTempDao();

    gradeCacheTempDao.deleteAll();
    int studentEmailIndex = 0;
    int studentFullNameIndex = 1;
    int emailNameOffset = 2;
    List<List<Object>> sheetValues = valueRange.getValues();
    List<String> headers = sheetValues.get(0).stream().map(Object::toString).collect(Collectors.toList());
    List<String> hwTitles = headers.subList(2, headers.size());
    sheetValues.remove(0); // remove headers
    sheetValues = sheetValues.stream().map(row -> standardizeSize(row, headers.size())).collect(Collectors.toList()); // standardize sizes of rows

    for (List<Object> row : sheetValues) {
      if (row.isEmpty()) {
        continue;
      }
      String studentEmail = row.get(studentEmailIndex).toString();
      String studentFullName = row.get(studentFullNameIndex).toString();
      checkIfStudentIsLecturer(studentEmail, personCacheTempDao);
      Person student = new Person(studentEmail, studentFullName, true, false, false);
      personCacheTempDao.savePerson(student);
      for (int hwTitleIndex = 0; hwTitleIndex < hwTitles.size(); hwTitleIndex++) {

        if (hwTitles.get(hwTitleIndex).isEmpty()) { // ignore empty columns
          continue;
        }
        Optional<Homework> homeworkOpt = homeworkCacheTempDao.getHomeworkByLectureTitle(hwTitles.get(hwTitleIndex));
        if (homeworkOpt.isEmpty()) {
          throw new MapSheetToCacheException("No homework with lecture title: " + hwTitles.get(hwTitleIndex));
        }

        String gradeValue = row.get(hwTitleIndex + emailNameOffset).toString();

        if (!StringUtils.isNumeric(gradeValue)) {
          continue;
        }
        try {
          gradeCacheTempDao.saveGrade(
            student.getEmail(),
            homeworkOpt.get().getLecture().getTitle(),
            Integer.parseInt(gradeValue)
          );
        } catch (LmsValueNotFoundException e) {
          throw new MapSheetToCacheException(e);
        }
      }
    }
  }

  private void checkIfStudentIsLecturer(String studentEmail, PersonCacheDao personCacheTempDao) throws MapSheetToCacheException {
    Optional<Person> studentOpt = personCacheTempDao.getPersonByEmail(studentEmail);
    if (studentOpt.isPresent()) {
      if (studentOpt.get().isLecturer()) {
        throw new MapSheetToCacheException("Wrong student. There is already lecturer with email: " + studentEmail);
      }
    }
  }

  private List<Object> standardizeSize(List<Object> row, int standardSize) {
    row.addAll(Collections.nCopies(standardSize - row.size(), ""));
    return row;
  }

  @Override
  public ValueRange mapCacheToSheet(CacheDaoManager cacheDaoManager) {
    PersonCacheDao personCacheDao = cacheDaoManager.getPersonDao();
    HomeworkCacheDao homeworkCacheDao = cacheDaoManager.getHwDao();
    GradeCacheDao gradeCacheDao = cacheDaoManager.getGradeDao();

    List<Object> columnHomeworks = new ArrayList<>(headers);
    List<List<Object>> valuesToFlush = new ArrayList<>(new ArrayList<>());

    List<String> hwLectureTitles = homeworkCacheDao.getHomeworksSortedByDeadline().stream()
      .map(hw -> hw.getLecture().getTitle()).collect(Collectors.toList());

    List<String> hwLectureTitlesLinks = homeworkCacheDao.getHomeworksSortedByDeadline().stream()
      .map((Homework homework) -> linkToHwTitleCell(homework, homeworkCacheDao)).collect(Collectors.toList());

    valuesToFlush.add(columnHomeworks);
    valuesToFlush.get(0).addAll(hwLectureTitlesLinks);

    for (Person student : personCacheDao.getStudents()) {
      List<Object> rowGrades = new ArrayList<>(columnHomeworks.size());
      rowGrades.add(student.getEmail());
      rowGrades.add(student.getFullName());
      for (String hwTitle : hwLectureTitles) {
        Optional<Grade> gradeOpt = gradeCacheDao.getGradeByStudentByHomework(student.getEmail(), hwTitle);
        rowGrades.add(gradeOpt.isPresent() ? gradeOpt.get().getValue() : "");
      }
      valuesToFlush.add(rowGrades);
    }
    return new ValueRange().setRange(getSheetTitle()).setValues(valuesToFlush);
  }

  private String linkToHwTitleCell(Homework homework, HomeworkCacheDao homeworkCacheDao) {
    int indexOffset = 2; // headers + sheet first index is 1

    int rowIndex = homeworkCacheDao.getHomeworksSortedByDeadline().indexOf(homework) + indexOffset;
    String columnLetter = homeworksSheetTitlesRange.substring(homeworksSheetTitlesRange.length() - 1);
    String sheetTitle = homeworksSheetTitlesRange.split("!")[0];
    return String.format("%s!%s%d", sheetTitle, columnLetter, rowIndex);
  }

  @Override
  public List<Request> initialRequestsToExecute(SheetsClient client) {
    int minGrade = 1;
    int maxGrade = 10;

    int hwTitlesRowIndex = 0;
    GridRange hwTitlesGridRange = new GridRange()
      .setSheetId(client.getSheetIdByTitle(getSheetTitle()))
      .setStartColumnIndex(headers.size())
      .setStartRowIndex(hwTitlesRowIndex).setEndRowIndex(hwTitlesRowIndex + 1);

    return List.of(
      client.dataValidationRequest(
        new GridRange().setStartColumnIndex(headers.size()).setStartRowIndex(1).setSheetId(client.getSheetIdByTitle(getSheetTitle())),
        "ONE_OF_LIST",
        IntStream.range(minGrade, maxGrade + 1)
          .mapToObj(String::valueOf)
          .map(v -> new ConditionValue().setUserEnteredValue(v))
          .collect(Collectors.toList())),
      client.dataValidationRequest(
        hwTitlesGridRange,
        "ONE_OF_RANGE",
        List.of(new ConditionValue().setUserEnteredValue(homeworksSheetTitlesRange)))
    );
  }

  @Override
  public String getSheetTitle() {
    return sheetName;
  }
}
