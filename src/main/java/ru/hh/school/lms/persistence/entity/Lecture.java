package ru.hh.school.lms.persistence.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Objects;

public class Lecture implements Comparable<Lecture> {
  private static final Logger LOGGER = LoggerFactory.getLogger(Lecture.class);

  // id (unique)
  private String title;
  private LocalDate date;
  private Person lecturer;
  private String specialization;
  private boolean hw;

  public Lecture(String title, LocalDate date, Person lecturer, String specialization, boolean hw) {
    this.specialization = specialization;
    if (!lecturer.isLecturer()) {
      LOGGER.error("Person is not a lecturer: " + lecturer);
    }
    this.title = title;
    this.date = date;
    this.lecturer = lecturer;
    this.hw = hw;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Person getLecturer() {
    return lecturer;
  }

  public void setLecturer(Person lecturer) {
    this.lecturer = lecturer;
  }

  public String getSpecialization() {
    return specialization;
  }

  public void setSpecialization(String specialization) {
    this.specialization = specialization;
  }

  public boolean isHw() {
    return hw;
  }

  public void setHw(boolean hw) {
    this.hw = hw;
  }

  @Override
  public int compareTo(Lecture o) {
    return date.compareTo(o.date);
  }

  @Override
  public String toString() {
    return "Lecture{" +
      "title='" + title + '\'' +
      ", date=" + date +
      ", lecturer=" + lecturer +
      (hw ? ", with homework" : "") +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Lecture)) {
      return false;
    }
    Lecture lecture = (Lecture) o;
    return title.equals(lecture.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title);
  }
}
