package ru.hh.school.lms.persistence.sheets.relation;

import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.ValueRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.cache.CacheDaoManager;
import ru.hh.school.lms.persistence.cache.LectureCacheDao;
import ru.hh.school.lms.persistence.cache.PersonCacheDao;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.persistence.entity.Person;
import ru.hh.school.lms.sheets.SheetsClient;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@Component
@PropertySource(value = "classpath:sheets/labels.properties", encoding = "UTF-8")
public class LecturesSheetRelation implements GoogleSheetRelation {
  private Function<String, LocalDate> dateParser;

  @Value("#{'${lectures.sheet.headers}'.split(',')}")
  private List<Object> headers;

  @Value("${lectures.sheet.name}")
  private String sheetName;

  private final static int DATE_INDEX = 0;
  private final static int TITLE_INDEX = 1;
  private final static int LECTURER_FULL_NAME_INDEX = 2;
  private final static int LECTURER_EMAIL_INDEX = 3;
  private final static int SPECIALIZATION_INDEX = 4;
  private final static int HOMEWORK_INDEX = 5;

  @Autowired
  public LecturesSheetRelation(Function<String, LocalDate> dateParser) {
    this.dateParser = dateParser;
  }

  @Override
  public void mapSheetToCache(ValueRange valueRange, CacheDaoManager cacheDaoManager) throws MapSheetToCacheException {
    PersonCacheDao personCacheTempDao = cacheDaoManager.getPersonTempDao();
    LectureCacheDao lectureCacheTempDao = cacheDaoManager.getLectureTempDao();
    personCacheTempDao.deleteAll();
    lectureCacheTempDao.deleteAll();
    List<List<Object>> values = valueRange.getValues();
    values.remove(0); // remove headers
    for (List<Object> row : values) {
      if (row.isEmpty()) {
        continue;
      }
      Lecture lectureToSave = mapRowToLecture(row, personCacheTempDao);
      try {
        lectureCacheTempDao.saveLecture(lectureToSave);
      } catch (LmsValueNotFoundException e) {
        throw new MapSheetToCacheException(e);
      }
    }
  }

  private Lecture mapRowToLecture(List<Object> row, PersonCacheDao personCacheTempDao) throws MapSheetToCacheException {
    int rowSize = row.size();
    for (int i = 0; i < headers.size() - rowSize; i++) {
      row.add("");
    }
    if (row.get(TITLE_INDEX).toString().isEmpty() ||
      row.get(DATE_INDEX).toString().isEmpty()) {
      throw new MapSheetToCacheException("Empty Title or Date at Lectures Sheet.");
    }
    String lecturerEmail = row.get(LECTURER_EMAIL_INDEX).toString();
    String lectureFullName = row.get(LECTURER_FULL_NAME_INDEX).toString();

    Person lecturer = new Person(lecturerEmail, lectureFullName, false, true, false);
    personCacheTempDao.savePerson(lecturer);
    return new Lecture(
      row.get(TITLE_INDEX).toString(),
      dateParser.apply(row.get(DATE_INDEX).toString()),
      lecturer,
      row.get(SPECIALIZATION_INDEX).toString(),
      !row.get(HOMEWORK_INDEX).toString().isEmpty()
    );
  }

  @Override
  public ValueRange mapCacheToSheet(CacheDaoManager cacheDaoManager) {
    LectureCacheDao lectureCacheDao = cacheDaoManager.getLectureDao();
    List<List<Object>> valuesToFlush = new ArrayList<>(Collections.singletonList(headers));
    for (Lecture lec : lectureCacheDao.getLecturesSorted()) {
      List<Object> row = new ArrayList<>(headers); // init with some values
      row.set(DATE_INDEX, lec.getDate().toString());
      row.set(TITLE_INDEX, lec.getTitle());
      row.set(LECTURER_FULL_NAME_INDEX, lec.getLecturer().getFullName());
      row.set(LECTURER_EMAIL_INDEX, lec.getLecturer().getEmail());
      row.set(SPECIALIZATION_INDEX, lec.getSpecialization());
      row.set(HOMEWORK_INDEX, lec.isHw() ? "Дз будет" : "");
      valuesToFlush.add(row);
    }
    return new ValueRange().setValues(valuesToFlush).setRange(getSheetTitle());
  }

  @Override
  public List<Request> initialRequestsToExecute(SheetsClient client) {
    return List.of(
      client.dateRenderRequest(DATE_INDEX, client.getSheetIdByTitle(getSheetTitle())),
      client.dateValidationRequest(DATE_INDEX, client.getSheetIdByTitle(getSheetTitle()))
    );
  }

  @Override
  public String getSheetTitle() {
    return sheetName;
  }

}
