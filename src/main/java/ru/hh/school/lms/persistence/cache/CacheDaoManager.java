package ru.hh.school.lms.persistence.cache;

public final class CacheDaoManager {
  private final GradeCacheDao gradeDao;
  private final HomeworkCacheDao hwDao;
  private final LectureCacheDao lectureDao;
  private final PersonCacheDao personDao;

  // temporary
  private final GradeCacheDao gradeTempDao;
  private final HomeworkCacheDao hwTempDao;
  private final LectureCacheDao lectureTempDao;
  private final PersonCacheDao personTempDao;

  private DataSource mainCache;
  private DataSource tempCache;

  public CacheDaoManager() {
    mainCache = new DataSource();
    tempCache = new DataSource();

    this.personDao = new PersonCacheDao(mainCache);
    this.lectureDao = new LectureCacheDao(personDao, mainCache);
    this.hwDao = new HomeworkCacheDao(lectureDao, mainCache);
    this.gradeDao = new GradeCacheDao(personDao, hwDao, mainCache);

    this.personTempDao = new PersonCacheDao(tempCache);
    this.lectureTempDao = new LectureCacheDao(personTempDao, tempCache);
    this.hwTempDao = new HomeworkCacheDao(lectureTempDao, tempCache);
    this.gradeTempDao = new GradeCacheDao(personTempDao, hwTempDao, tempCache);
  }

  public void saveTemporaryCache() {
    mainCache.replaceWith(tempCache);
    tempCache.clear();
  }

  public GradeCacheDao getGradeDao() {
    return gradeDao;
  }

  public HomeworkCacheDao getHwDao() {
    return hwDao;
  }

  public LectureCacheDao getLectureDao() {
    return lectureDao;
  }

  public PersonCacheDao getPersonDao() {
    return personDao;
  }

  public GradeCacheDao getGradeTempDao() {
    return gradeTempDao;
  }

  public HomeworkCacheDao getHwTempDao() {
    return hwTempDao;
  }

  public LectureCacheDao getLectureTempDao() {
    return lectureTempDao;
  }

  public PersonCacheDao getPersonTempDao() {
    return personTempDao;
  }
}
