package ru.hh.school.lms.persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import ru.hh.school.lms.persistence.cache.CacheDaoManager;
import ru.hh.school.lms.persistence.sheets.GoogleSheetsSynchronizer;
import ru.hh.school.lms.persistence.sheets.impl.GradeSheetsDao;
import ru.hh.school.lms.persistence.sheets.impl.HomeworkSheetsDao;
import ru.hh.school.lms.persistence.sheets.impl.LectureSheetsDao;
import ru.hh.school.lms.persistence.sheets.impl.PersonSheetsDao;

@Configuration
public class DataLmsConfig {

  // Cache DAOs
  @Bean
  CacheDaoManager cacheDaoManager() {
    return new CacheDaoManager();
  }

  // Sheets DAOs

  @Bean
  @Primary
  GradeSheetsDao gradeSheetsDao(CacheDaoManager cacheDaoManager, GoogleSheetsSynchronizer synchronizer) {
    return new GradeSheetsDao(cacheDaoManager.getGradeDao(), synchronizer);
  }

  @Bean
  @Primary
  HomeworkSheetsDao homeworkSheetsDao(CacheDaoManager cacheDaoManager, GoogleSheetsSynchronizer synchronizer) {
    return new HomeworkSheetsDao(cacheDaoManager.getHwDao(), synchronizer);
  }

  @Bean
  @Primary
  LectureSheetsDao lectureSheetsDao(CacheDaoManager cacheDaoManager) {
    return new LectureSheetsDao(cacheDaoManager.getLectureDao());
  }

  @Bean
  @Primary
  PersonSheetsDao personSheetsDao(CacheDaoManager cacheDaoManager) {
    return new PersonSheetsDao(cacheDaoManager.getPersonDao());
  }
}
