package ru.hh.school.lms.persistence.cache;

import ru.hh.school.lms.persistence.GradeDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.entity.Grade;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class GradeCacheDao implements GradeDao {

  private final Map<Person, Map<Homework, Grade>> gradesMap;
  private PersonCacheDao personCacheDao;
  private HomeworkCacheDao homeworkCacheDao;

  public GradeCacheDao(PersonCacheDao personCacheDao, HomeworkCacheDao homeworkCacheDao, DataSource cache) {
    this.personCacheDao = personCacheDao;
    this.homeworkCacheDao = homeworkCacheDao;
    gradesMap = cache.getGradesMap();
  }

  @Override
  public void saveGrade(String studentEmail, String lectureTitleOfHw, Integer gradeValue) throws LmsValueNotFoundException {
    Person student = getStudentByEmailIfExist(studentEmail); // check if student with email exist
    Homework homework = getHomeworkByLectureTitleIfExist(lectureTitleOfHw); // check if homework with title exist
    if (gradesMap.containsKey(student)) {
      gradesMap.get(student).put(homework, new Grade(student, homework, gradeValue));
      return;
    }
    putGradeToNewStudent(student, homework, gradeValue);
  }

  private Person getStudentByEmailIfExist(String studentEmail) throws LmsValueNotFoundException {
    Optional<Person> studentOpt = personCacheDao.getPersonByEmail(studentEmail);
    return studentOpt.orElseThrow(() -> new LmsValueNotFoundException("No student found with email: " + studentEmail));
  }

  private Homework getHomeworkByLectureTitleIfExist(String lectureTitleOfHw) throws LmsValueNotFoundException {
    Optional<Homework> homeworkOpt = homeworkCacheDao.getHomeworkByLectureTitle(lectureTitleOfHw);
    return homeworkOpt.orElseThrow(() -> new LmsValueNotFoundException("No homework found with lecture title: " + lectureTitleOfHw));
  }

  @Override
  public List<Grade> getStudentGrades(String studentEmail) {
    Optional<Person> studentOpt = personCacheDao.getPersonByEmail(studentEmail);
    if (studentOpt.isPresent() && studentOpt.get().isStudent()) {
      Map<Homework, Grade> studentGrades = gradesMap.get(studentOpt.get());
      return new ArrayList<>(studentGrades != null ? studentGrades.values() : Collections.emptyList());
    }
    return Collections.emptyList();
  }

  @Override
  public Optional<Grade> getGradeByStudentByHomework(String studentEmail, String hwLectureTitle) {
    List<Grade> grades = getStudentGrades(studentEmail);
    for (Grade grade : grades) {
      if (grade.getHomework().getLecture().getTitle().equals(hwLectureTitle)) {
        return Optional.of(grade);
      }
    }
    return Optional.empty();
  }

  private void putGradeToNewStudent(Person student, Homework hwOfGrade, Integer gradeValue) {
    Map<Homework, Grade> studentGrades = new ConcurrentHashMap<>(1);
    studentGrades.put(hwOfGrade, new Grade(student, hwOfGrade, gradeValue));
    gradesMap.put(student, studentGrades);
  }

  public void deleteAll() {
    gradesMap.clear();
  }
}
