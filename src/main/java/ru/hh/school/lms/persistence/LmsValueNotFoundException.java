package ru.hh.school.lms.persistence;

public class LmsValueNotFoundException extends Exception {
  public LmsValueNotFoundException(String message) {
    super(message);
  }

  public LmsValueNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public LmsValueNotFoundException(Throwable cause) {
    super(cause);
  }
}
