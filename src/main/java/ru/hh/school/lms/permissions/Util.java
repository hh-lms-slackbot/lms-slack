package ru.hh.school.lms.permissions;

import com.google.api.services.drive.model.Permission;

import static ru.hh.school.lms.permissions.GoogleRole.READER;
import static ru.hh.school.lms.permissions.GoogleRole.WRITER;

final class Util {

  private static final String TYPE_ANYONE = "anyone";

  // non-instantiable util class
  private Util() {
  }

  static final String SERVICE_ACCOUNT_DOMAIN = "gserviceaccount.com";
  private static final String TYPE_USER = "user";

  static Permission writer(String email) {
    return permission(email, WRITER);
  }

  static Permission reader(String email) {
    return permission(email, READER);
  }

  private static Permission permission(String email, GoogleRole role) {
    return new Permission()
      .setEmailAddress(email)
      .setType(TYPE_USER)
      .setRole(role.toString());
  }

  static Permission readerForAnyone() {
    return new Permission().setRole(READER.toString()).setType(TYPE_ANYONE);
  }
}
