package ru.hh.school.lms.permissions;

import com.github.seratch.jslack.api.model.event.MemberJoinedChannelEvent;
import com.github.seratch.jslack.api.model.event.MemberLeftChannelEvent;
import com.github.seratch.jslack.api.rtm.RTMEventHandler;
import com.google.api.services.drive.Drive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.hh.school.lms.slack.SlackHelper;

import java.util.Locale;
import java.util.Properties;

@Configuration
class PermissionsConfig {

  @Autowired
  private Locale locale;

  @Bean
  Permissions permissions(ApplicationContext thisContext, SlackHelper slack, Drive googleDrive, @Qualifier("serviceProperties") Properties settings) {
    return new Permissions(thisContext, slack, googleDrive, settings);
  }

  @Bean
  RTMEventHandler<MemberJoinedChannelEvent> memberJoined(ApplicationContext thisContext, SlackHelper slack,
                                                         Drive googleDrive, @Qualifier("serviceProperties") Properties settings) {
    Permissions permissions = permissions(thisContext, slack, googleDrive, settings);
    return new RTMEventHandler<>() {
      @Override
      public void handle(MemberJoinedChannelEvent event) {
        permissions.memberJoined(event.getUser(), event.getChannel());
      }
    };
  }

  @Bean
  RTMEventHandler<MemberLeftChannelEvent> memberLeft(ApplicationContext thisContext, SlackHelper slack,
                                                     Drive googleDrive, @Qualifier("serviceProperties") Properties settings) {
    Permissions permissions = permissions(thisContext, slack, googleDrive, settings);
    return new RTMEventHandler<>() {
      @Override
      public void handle(MemberLeftChannelEvent event) {
        permissions.memberLeft(event.getUser(), event.getChannel());
      }
    };
  }
}
