package ru.hh.school.lms.permissions;

enum GoogleRole {
  OWNER("owner"),
  ORGANIZER("organizer"),
  FILE_ORGANIZER("fileOrganizer"),
  WRITER("writer"),
  COMMENTER("commenter"),
  READER("reader");

  private final String value;

  GoogleRole(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return value;
  }

  static GoogleRole fromString(String string) {
    for (GoogleRole role : GoogleRole.values()) {
      if (role.value.equals(string)) {
        return role;
      }
    }
    throw new IllegalArgumentException("Illegal role value: " + string);
  }
}
