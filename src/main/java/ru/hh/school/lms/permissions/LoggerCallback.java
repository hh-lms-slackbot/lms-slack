package ru.hh.school.lms.permissions;

import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class LoggerCallback<T> extends JsonBatchCallback<T> {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoggerCallback.class);

  private final String action;
  private final GoogleRole role;
  private final String email;
  private final String sheetUrl;

  LoggerCallback(String action, GoogleRole role, String email, String sheetUrl) {
    this.action = action;
    this.role = role;
    this.email = email;
    this.sheetUrl = sheetUrl;
  }

  @Override
  public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
    LOGGER.error(String.format("Failed to %s permission '%s' for '%s' url = %s",
      action, role, email, sheetUrl));

  }

  @Override
  public void onSuccess(T object, HttpHeaders responseHeaders) {
    logSuccess(action, role, email, sheetUrl);
  }

  static void logSuccess(String action, GoogleRole role, String email, String sheetUrl) {
    LOGGER.info(String.format(
      "Successfully %sd permission '%s' for '%s' url = %s",
      action, role, email, sheetUrl));
  }
}
