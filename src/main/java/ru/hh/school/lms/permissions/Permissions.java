package ru.hh.school.lms.permissions;

import com.github.seratch.jslack.api.model.User;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import ru.hh.school.lms.slack.SlackHelper;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;
import static ru.hh.school.lms.permissions.GoogleRole.OWNER;
import static ru.hh.school.lms.permissions.GoogleRole.READER;
import static ru.hh.school.lms.permissions.GoogleRole.WRITER;
import static ru.hh.school.lms.permissions.Util.SERVICE_ACCOUNT_DOMAIN;
import static ru.hh.school.lms.permissions.Util.readerForAnyone;
import static ru.hh.school.lms.permissions.Util.writer;

final class Permissions {

  private static final Logger LOGGER = LoggerFactory.getLogger(Permissions.class);
  private static final String DELETE = "delete";
  private static final String CREATE = "create";
  private static final String EMAIL_ANYONE = "ANYONE";
  private static final int DEFAULT_SHEET = 0;

  private final ApplicationContext thisContext;
  private final SlackHelper slack;
  private final Drive googleDrive;
  private final String spreadsheetId;
  private final String adminChannelId;
  private final String sheetsUrlEditTemplate;

  Permissions(ApplicationContext thisContext,
              SlackHelper slack,
              Drive googleDrive,
              Properties properties) {
    this.thisContext = thisContext;
    this.slack = slack;
    this.googleDrive = googleDrive;
    this.sheetsUrlEditTemplate = properties.getProperty("sheets.url.template.edit");
    this.spreadsheetId = properties.getProperty("ru.hh.school.lms.sheets.spreadsheetId");
    String adminChannelName = properties.getProperty("ru.hh.school.lms.role.admin.channel");
    this.adminChannelId = slack.getConversationByName(adminChannelName)
      .orElseThrow(() -> new RuntimeException("Can't get general channel which is called " + adminChannelName +
        "Please, create a channel with specified name.")).getId();
  }

  @EventListener
  public void checkInitialPermissions(ContextRefreshedEvent event) throws IOException {
    if (!thisContext.equals(event.getApplicationContext())) {
      return;
    }

    List<User> allUsers = slack.getAllUsers();
    Set<String> adminIds = slack.getMembers(adminChannelId);

    Set<String> mustBeWriterEmails = allUsers.stream()
      .filter(user -> adminIds.contains(user.getId())) // admins that
      .filter(not(User::isBot)) // are not a bot
      .map(user -> user.getProfile().getEmail())// has email
      .filter(Objects::nonNull) // that is not null
      .collect(Collectors.toSet());

    BatchRequest batch = googleDrive.batch();
    googleDrive.permissions()
      .create(spreadsheetId, readerForAnyone())
      .setSendNotificationEmail(false)
      .queue(batch, new LoggerCallback<>(CREATE, READER, EMAIL_ANYONE, sheetUrl()));

    List<Permission> permissions = getPermissions();

    for (Permission p : permissions) {
      String email = p.getEmailAddress();
      GoogleRole role = GoogleRole.fromString(p.getRole());
      if (role == OWNER) {
        mustBeWriterEmails.remove(email);
        continue;
      }
      if (email == null && role == READER) { // 'reader' permissions for everyone
        continue;
      }
      if (email != null && email.endsWith(SERVICE_ACCOUNT_DOMAIN)) {
        continue;
      }
      if (role == WRITER && mustBeWriterEmails.remove(email)) {
        continue;
      }
      LoggerCallback<Void> deleteCallback = new LoggerCallback<>(DELETE, role, email, sheetUrl());
      googleDrive.permissions().delete(spreadsheetId, p.getId()).queue(batch, deleteCallback);
    }
    for (String email : mustBeWriterEmails) {
      LoggerCallback<Permission> createWriterCallback = new LoggerCallback<>(CREATE, WRITER, email, sheetUrl());
      googleDrive.permissions().create(spreadsheetId, writer(email)).queue(batch, createWriterCallback);
    }
    if (batch.size() > 0) {
      batch.execute();
    } else {
      LOGGER.info("Startup check: no Google Sheet permissions changes.");
    }
  }

  void memberLeft(String userId, String channel) {
    Optional<User> u = slack.getUser(userId);
    if (u.isPresent() && channel.equals(adminChannelId)) {
      adminLeftChannel(u.get());
    }
  }

  private void adminLeftChannel(User user) {
    doTry(() -> {
      String email = user.getProfile().getEmail();
      BatchRequest batch = googleDrive.batch();
      List<Permission> permissions = getPermissions();
      for (Permission p : permissions) {
        GoogleRole role = GoogleRole.fromString(p.getRole());
        if (email.equals(p.getEmailAddress()) && role == WRITER) {
          googleDrive.permissions().delete(spreadsheetId, p.getId())
            .queue(batch, new LoggerCallback<>(DELETE, WRITER, email, sheetUrl()));
        }
      }
      if (batch.size() > 0) {
        batch.execute();
      }
      return null;
    });
  }

  void memberJoined(String userId, String channel) {
    Optional<User> user = slack.getUser(userId);
    if (user.isPresent()) {
      if (channel.equals(adminChannelId)) {
        adminJoinedChannel(user.get());
      }
    }
  }

  private void adminJoinedChannel(User user) {
    doTry(() -> {
      String email = user.getProfile().getEmail();
      googleDrive.permissions().create(spreadsheetId, writer(email)).execute();
      LoggerCallback.logSuccess(CREATE, WRITER, email, sheetUrl());
      return null;
    });
  }

  private List<Permission> getPermissions() throws IOException {
    PermissionList permissionList = googleDrive.permissions().list(spreadsheetId)
      .setFields("permissions(emailAddress,id,kind,role,type)").execute();
    return permissionList.getPermissions();
  }

  private <T> void doTry(Callable<T> callback) {
    try {
      callback.call();
    } catch (Exception exc) {
      LOGGER.error("An error occurred during GoogleSheets permissions changing.", exc);
      throw new RuntimeException(exc);
    }
  }

  private String sheetUrl() {
    return String.format(sheetsUrlEditTemplate, spreadsheetId, DEFAULT_SHEET);
  }
}
