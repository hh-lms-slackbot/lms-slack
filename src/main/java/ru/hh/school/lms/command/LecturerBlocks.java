package ru.hh.school.lms.command;

import com.github.seratch.jslack.api.model.block.ActionsBlock;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.api.model.block.element.ButtonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.grading.GradeFlow;
import ru.hh.school.lms.homework.HomeworkFlow;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;
import ru.hh.school.lms.slack.event.EventHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.InteractionEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

final class LecturerBlocks {

  private static final Logger LOGGER = LoggerFactory.getLogger(LecturerBlocks.class);

  private HomeworkFlow homeworkFlow;
  private GradeFlow gradeFlow;
  private final CallbackDispatcher dispatcher;
  private MessageManager messageManager;
  private final Properties settings;
  private final HeaderBlocks headerBlocks;

  LecturerBlocks(HomeworkFlow homeworkFlow,
                        GradeFlow gradeFlow,
                        CallbackDispatcher dispatcher,
                        MessageManager messageManager,
                        Properties settings,
                        HeaderBlocks headerBlocks) {

    this.homeworkFlow = homeworkFlow;
    this.gradeFlow = gradeFlow;
    this.dispatcher = dispatcher;
    this.messageManager = messageManager;
    this.settings = settings;
    this.headerBlocks = headerBlocks;
  }


  List<LayoutBlock> getBlocks() throws EventHandlingException {
    String createNewTaskCallbackId = staticId("create_new_task_callback");
    String selectStudentGradeCallbackId = staticId("select_grade_callback");

    ActionsBlock buttonsBlock = ActionsBlock.builder()
      .elements(List.of(
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(messageManager.of("for_lecturer_new_grade"))
            .build())
          .actionId(callback(selectStudentGradeCallbackId, gradeFlow::show))
          .build(),
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(messageManager.of("for_lecturer_new_task"))
            .build())
          .actionId(callback(createNewTaskCallbackId, homeworkFlow::show))
          .build(),
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(messageManager.of("for_lecturer_help"))
            .build())
          .url(settings.getProperty("ru.hh.school.lms.lecturer.help"))
          .build()
      ))
      .build();

    List<LayoutBlock> result = new ArrayList<LayoutBlock>();
    result.addAll(headerBlocks.getBlocks());
    result.add(buttonsBlock);
    return result;
  }

  private <T extends InteractionEvent> String callback(String callbackId, EventHandler<T> callback) {
    dispatcher.register(callbackId, callback);
    return callbackId;
  }

  private String staticId(String name) {
    return String.format("%s.%s", this.getClass().getCanonicalName(), name);
  }
}
