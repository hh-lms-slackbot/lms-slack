package ru.hh.school.lms.command;

import com.github.seratch.jslack.api.model.User;
import com.github.seratch.jslack.api.model.block.ActionsBlock;
import com.github.seratch.jslack.api.model.block.DividerBlock;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.api.model.block.element.ButtonElement;
import com.github.seratch.jslack.api.model.block.element.ImageElement;
import com.github.seratch.jslack.app_backend.interactive_messages.response.ActionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.persistence.GradeDao;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.persistence.PersonDao;
import ru.hh.school.lms.persistence.entity.Grade;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;
import ru.hh.school.lms.slack.event.BlockActionEvent;
import ru.hh.school.lms.slack.event.EventHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.InteractionEvent;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;

final class StudentBlocks {

  private static final Logger LOGGER = LoggerFactory.getLogger(StudentBlocks.class);

  private SlackHelper slack;
  private final CallbackDispatcher dispatcher;
  private MessageManager message;
  private final LectureDao lectureDao;
  private final HomeworkDao homeworkDao;
  private final GradeDao gradeDao;
  private final PersonDao personDao;
  private final HeaderBlocks headerBlocks;
  private final Function<LocalDate, String> dateFormatter;

  private final String adminChannelId;

  private static final String BLOCK_SELECT_SCHEDULE = "select_schedule";
  private static final String BLOCK_SELECT_HOMEWORKS = "select_homeworks";
  private static final String BLOCK_SELECT_GRADES = "select_grades";
  private static final int LIMIT_OF_NEXT_LECTURES_TO_SHOW = 3;

  StudentBlocks(SlackHelper slack,
                CallbackDispatcher dispatcher,
                MessageManager message,
                LectureDao lectureDao,
                HomeworkDao homeworkDao,
                GradeDao gradeDao,
                PersonDao personDao,
                HeaderBlocks headerBlocks,
                Function<LocalDate, String> dateFormatter,
                Properties settings) {
    this.slack = slack;
    this.dispatcher = dispatcher;
    this.message = message;
    this.lectureDao = lectureDao;
    this.homeworkDao = homeworkDao;
    this.gradeDao = gradeDao;
    this.personDao = personDao;
    this.headerBlocks = headerBlocks;
    this.dateFormatter = dateFormatter;
    String adminChannelName = settings.getProperty("ru.hh.school.lms.role.admin.channel");
    this.adminChannelId = slack.getConversationByName(adminChannelName)
      .orElseThrow(() -> new RuntimeException("Can't get general channel which is called " + adminChannelName +
        "Please, create a channel with specified name.")).getId();
  }

  List<LayoutBlock> getBlocks(String userId) throws EventHandlingException {
    return studentMessageLayoutBlocks(null, userId);
  }

  private List<LayoutBlock> studentMessageLayoutBlocks(List<LayoutBlock> additionalBlocks, String userId) {
    ActionsBlock buttonsBlock = ActionsBlock.builder()
      .elements(List.of(
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(message.of("for_student_schedule"))
            .build())
          .actionId(callback(staticId(BLOCK_SELECT_SCHEDULE), this::showShedule))
          .build(),
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(message.of("for_student_homeworks"))
            .build())
          .actionId(callback(staticId(BLOCK_SELECT_HOMEWORKS), this::showHomeworks))
          .build(),
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(message.of("for_student_grades"))
            .build())
          .actionId(callback(staticId(BLOCK_SELECT_GRADES + userId), this::showGrades))
          .build()
      ))
      .build();

    List<LayoutBlock> blocks = new ArrayList<>(headerBlocks.getBlocks());
    blocks.add(buttonsBlock);
    if (additionalBlocks != null) {
      blocks.addAll(additionalBlocks);
    }
    return blocks;
  }

  private void showShedule(BlockActionEvent event) {
    List<LayoutBlock> lectures = lectureDao.getLecturesSorted().stream()
      .filter(lecture -> lecture.getDate().isAfter(LocalDate.now().minusDays(1))) // next lectures
      .map(this::lectureToLayoutBlocks)
      .limit(LIMIT_OF_NEXT_LECTURES_TO_SHOW)
      .flatMap(List::stream)
      .collect(Collectors.toList());
    addInfoToMsg(event, lectures);
  }

  private void showHomeworks(BlockActionEvent event) {
    List<LayoutBlock> homeworks = homeworkDao.getHomeworksSortedByDeadline().stream()
      .filter(homework -> homework.getDeadline().isAfter(LocalDate.now().minusDays(1))) // filter past deadlines
      .map(this::homeworkToLayoutBlocks)
      .flatMap(List::stream)
      .collect(Collectors.toList());
    addInfoToMsg(event, homeworks);
  }

  private void showGrades(BlockActionEvent event) {
    Optional<User> studentOpt = slack.getUser(event.getUser().getId());
    if (studentOpt.isEmpty()) {
      addInfoToMsg(event, toLayoutBlocks(Collections.singletonList(
        message.of("for_student_user_not_found_by_id")
      )));
      return;
    }
    String studentEmail = studentOpt.get().getProfile().getEmail();
    if (personDao.getPersonByEmail(studentEmail).isEmpty()) {
      String adminsStr = getAdminsStr();
      List<String> layoutBlocksStr = new ArrayList<>(List.of(
        String.format(message.of("for_student_email_not_found"), studentEmail)
      ));
      if (!adminsStr.isEmpty()) {
        layoutBlocksStr.add(message.of("for_student_email_not_found_admins_header") + "\n" + adminsStr);
      }
      addInfoToMsg(event, toLayoutBlocks(layoutBlocksStr));
      return;
    }
    List<Grade> grades = gradeDao.getStudentGrades(studentEmail);
    if (grades.isEmpty()) {
      addInfoToMsg(event, toLayoutBlocks(Collections.singletonList(
        String.format(message.of("for_student_have_no_grades"), studentEmail)
      )));
      return;
    }
    List<LayoutBlock> gradesToShow = grades.stream()
      .map(this::gradeToLayoutBlocks)
      .flatMap(List::stream)
      .collect(Collectors.toList());
    addInfoToMsg(event, gradesToShow);
  }

  private String getAdminsStr() {
    List<String> bots = slack.getAllUsers().stream().filter(User::isBot).map(User::getId).collect(Collectors.toList());
    return slack.getMembers(adminChannelId).stream()
      .filter(id -> !bots.contains(id)) // without bots
      .map(id -> String.format("<@%s>", id))
      .collect(Collectors.joining(" "));
  }

  private void addInfoToMsg(BlockActionEvent event, List<LayoutBlock> additionalBlocks) {
    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .blocks(studentMessageLayoutBlocks(additionalBlocks, event.getUser().getId()))
        .build()
    );
  }

  private List<LayoutBlock> lectureToLayoutBlocks(Lecture lecture) {
    Optional<User> lecturerOpt = slack.getUserByEmail(lecture.getLecturer().getEmail());
    // slack id or full name
    String lecturer = lecturerOpt.isEmpty() ? lecture.getLecturer().getFullName() : String.format("<@%s>", lecturerOpt.get().getId());
    return Arrays.asList(
      DividerBlock.builder().build(),
      SectionBlock.builder()
        .text(MarkdownTextObject.builder()
          .text(String.format("*%s*\n%s\n%s",
            lecture.getTitle(),
            dateFormatter.apply(lecture.getDate()),
            lecturer) +
            (lecture.isHw() ? "\nДЗ будет" : "")
          ).build())
        .accessory(lecturerOpt.map(user -> ImageElement.builder()
          .imageUrl(user.getProfile().getImage192())
          .altText(lecture.getLecturer().getFullName())
          .build()).orElse(null))
        .build());
  }

  private List<LayoutBlock> homeworkToLayoutBlocks(Homework homework) {
    String homeworkField = titleWithInternalLink(homework);
    return toLayoutBlocks(List.of(
      homeworkField,
      dateFormatter.apply(homework.getDeadline())
    ));
  }

  private List<LayoutBlock> gradeToLayoutBlocks(Grade grade) {
    return toLayoutBlocks(Arrays.asList(
      titleWithInternalLink(grade.getHomework()), String.valueOf(grade.getValue())
    ));
  }

  private List<LayoutBlock> toLayoutBlocks(List<String> values) {
    return Arrays.asList(
      DividerBlock.builder().build(),
      SectionBlock.builder()
        .fields(
          values.stream().map(v ->
            MarkdownTextObject.builder().text(v).build()
          ).collect(Collectors.toList())
        )
        .build()
    );
  }

  private String titleWithInternalLink(Homework homework) {
    return homework.getTaskLink().isEmpty() ?
      homework.getLecture().getTitle() : // only title if there is no link
      String.format("<%s|*%s*>", homework.getTaskLink(), homework.getLecture().getTitle()); // title with internal link
  }

  private <T extends InteractionEvent> String callback(String callbackId, EventHandler<T> callback) {
    dispatcher.register(callbackId, callback);
    return callbackId;
  }

  private String staticId(String name) {
    return this.getClass().getCanonicalName() + "." + name;
  }
}
