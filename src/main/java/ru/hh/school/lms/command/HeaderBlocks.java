package ru.hh.school.lms.command;

import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.OptionObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.api.model.block.element.OverflowMenuElement;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.persistence.LectureDao;
import ru.hh.school.lms.persistence.entity.Lecture;
import ru.hh.school.lms.sheets.SheetsClient;
import ru.hh.school.lms.slack.blockaction.UrlOptionObject;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

class HeaderBlocks {

  private final SheetsClient sheetsClient;
  private final String lecturesSheetName;
  private final String gradesSheetName;
  private final String homeworksSheetName;
  private final LectureDao lectureDao;
  private final String lecturesSheetUrl;
  private final String gradesSheetUrl;
  private final String homeworksSheetUrl;
  private final String sheetsUrlEditTemplate;
  private final String spreadsheetId;
  private final MessageManager messageManager;

  HeaderBlocks(MessageManager messageManager,
                      Properties settings,
                      SheetsClient sheetsClient,
                      LectureDao lectureDao) {
    this.sheetsUrlEditTemplate = settings.getProperty("sheets.url.template.edit");
    this.spreadsheetId = settings.getProperty("ru.hh.school.lms.sheets.spreadsheetId");
    this.sheetsClient = sheetsClient;
    this.messageManager = messageManager;
    this.lecturesSheetName = messageManager.of("lectures.sheet.name");
    this.gradesSheetName = messageManager.of("grades.sheet.name");
    this.homeworksSheetName = messageManager.of("homeworks.sheet.name");
    this.lectureDao = lectureDao;
    this.lecturesSheetUrl = getUrlForSheet(lecturesSheetName);
    this.gradesSheetUrl = getUrlForSheet(gradesSheetName);
    this.homeworksSheetUrl = getUrlForSheet(homeworksSheetName);
  }

  List<LayoutBlock> getBlocks() throws EventHandlingException {
    List<Lecture> allLectures = lectureDao.getLecturesSorted();
    String message;
    if (allLectures.isEmpty()) {
      message = messageManager.of("no_lectures");
    } else {
      double lectureIndex = findCurrentLecture(allLectures);
      if (lectureIndex < 0) {
        message = messageManager.of("all_lectures", allLectures.size());
      } else if (lectureIndex >= allLectures.size()) {
        message = messageManager.of("no_more_lectures", allLectures.size());
      } else if (lectureIndex > (int) lectureIndex) {
        message = messageManager.of("lectures_passed", (int) lectureIndex + 1, allLectures.size());
      } else {
        message = messageManager.of("lecture_today", (int) lectureIndex + 1, allLectures.size());
      }
    }
    LayoutBlock linksBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(message)
        .build())
      .accessory(OverflowMenuElement.builder()
        .options(List.of(
          new UrlOptionObject(OptionObject.builder()
            .value("lectures")
            .text(PlainTextObject.builder()
              .text(lecturesSheetName)
              .build())
            .build(), lecturesSheetUrl),
          new UrlOptionObject(OptionObject.builder()
            .value("grades")
            .text(PlainTextObject.builder()
              .text(gradesSheetName)
              .build())
            .build(), gradesSheetUrl),
          new UrlOptionObject(OptionObject.builder()
            .value("homeworks")
            .text(PlainTextObject.builder()
              .text(homeworksSheetName)
              .build())
            .build(), homeworksSheetUrl)
        )).actionId(staticId("sheets_link"))
        .build())
      .build();
    return Collections.singletonList(linksBlock);
  }

  private String staticId(String name) {
    return this.getClass().getCanonicalName() + "." + name;
  }

  private String getUrlForSheet(String sheetName) {
    Integer gid = sheetsClient.getSheetIdByTitle(sheetName);
    return String.format(sheetsUrlEditTemplate, spreadsheetId, gid);
  }

  private double findCurrentLecture(List<Lecture> allLectures) {
    LocalDate now = LocalDate.now();
    for (int i = 0; i < allLectures.size(); i++) {
      LocalDate lectureDate = allLectures.get(i).getDate();
      if (now.isBefore(lectureDate)) {
        return i - 0.5;
      }
      if (now.isEqual(lectureDate)) {
        return i;
      }
    }
    return allLectures.size();
  }

}
