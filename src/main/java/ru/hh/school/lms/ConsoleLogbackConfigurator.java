package ru.hh.school.lms;


import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import io.sentry.logback.SentryAppender;
import ru.hh.nab.logging.HhMultiAppender;
import ru.hh.nab.starter.NabLogbackBaseConfigurator;

import java.util.List;

/**
 * This class configures logger that prints to console in human readable format.
 * To set logger format use <code>log.pattern</code> in service.properties file.
 *
 * <p>
 * <i>Note:<i/> if you what to disable Logback self status logging, add the system property:
 * property <code>-Dlogback.statusListenerClass=ch.qos.logback.core.status.NopStatusListener</code>
 *
 * @see <a href="https://logback.qos.ch/manual/layouts.html#conversionWord">Conversion words in Logback patterns</a>
 */
public final class ConsoleLogbackConfigurator extends NabLogbackBaseConfigurator {

  private static final String DEFAULT_PATTERN = "%-12date{dd.MM.YYYY HH:mm:ss} [%-5level]  -  %msg%n";
  private static final String LOG_PATTERN_PROPERTY = "log.pattern";
  private static final String LMS_PACKAGE = "ru.hh.school.lms";
  private static final String APPENDER_NAME = "lms-console";
  private static final String SYSTEM_OUT = "System.out";
  private static final String SYSTEM_ERR = "System.err";

  private static final class LevelFilter extends AbstractMatcherFilter<ILoggingEvent> {

    private List<Level> levelsToKeep;

    private LevelFilter(Level... levelsToKeep) {
      this.levelsToKeep = List.of(levelsToKeep);
    }

    @Override
    public FilterReply decide(ILoggingEvent event) {
      if (levelsToKeep.contains(event.getLevel())) {
        return FilterReply.ACCEPT;
      } else {
        return FilterReply.DENY;
      }
    }
  }

  private ConsoleAppender setupAppender(LoggingContextWrapper context, Filter<ILoggingEvent> filter, String target) {
    ConsoleAppender<ILoggingEvent> newAppender = new ConsoleAppender<>();
    PatternLayoutEncoder logEncoder = new PatternLayoutEncoder();
    logEncoder.setContext(getContext());
    logEncoder.setPattern(context.getProperty(LOG_PATTERN_PROPERTY, DEFAULT_PATTERN));
    logEncoder.start();
    newAppender.setEncoder(logEncoder);
    newAppender.addFilter(filter);
    newAppender.setTarget(target);
    return newAppender;
  }

  @Override
  public void configure(LoggingContextWrapper context, HhMultiAppender service, HhMultiAppender libraries, SentryAppender sentry) {
    getRootLogger(context).setLevel(org.slf4j.event.Level.INFO);
    ConsoleAppender stdOutAppender = createAppender(context, APPENDER_NAME + "-" + SYSTEM_OUT,
      () -> setupAppender(context, new LevelFilter(Level.TRACE, Level.DEBUG, Level.INFO, Level.WARN), SYSTEM_OUT));
    ConsoleAppender stdErrAppender = createAppender(context, APPENDER_NAME + "-" + SYSTEM_ERR,
      () -> setupAppender(context, new LevelFilter(Level.ERROR), SYSTEM_ERR));
    createLogger(context, LMS_PACKAGE, org.slf4j.event.Level.DEBUG, false, List.of(stdOutAppender, stdErrAppender));
  }
}
