package ru.hh.school.lms.grading;

import ru.hh.school.lms.slack.flow.BaseFlowContext;

class GradeFlowContext extends BaseFlowContext {
  private String homework;
  private String student;
  private Integer grade;
  private Integer gradePrevious;

  GradeFlowContext() {
  }

  GradeFlowContext(GradeFlowContext context) {
    this.homework = context.homework;
    this.student = context.student;
    this.grade = context.grade;
    this.gradePrevious = context.gradePrevious;
  }

  String getHomework() {
    return homework;
  }

  void setHomework(String homework) {
    this.homework = homework;
  }

  String getStudent() {
    return student;
  }

  void setStudent(String student) {
    this.student = student;
  }

  Integer getGrade() {
    return grade;
  }

  void setGrade(Integer grade) {
    this.grade = grade;
  }

  Integer getGradePrevious() {
    return gradePrevious;
  }

  void setGradePrevious(Integer gradePrevious) {
    this.gradePrevious = gradePrevious;
  }

  boolean isGradeChanged() {
    return gradePrevious != null && !gradePrevious.equals(grade);
  }

  boolean isGradeUnchanged() {
    return gradePrevious != null && gradePrevious.equals(grade);
  }
}
