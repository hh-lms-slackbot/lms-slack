package ru.hh.school.lms.grading;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.User;
import com.github.seratch.jslack.api.model.block.ActionsBlock;
import com.github.seratch.jslack.api.model.block.DividerBlock;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.OptionGroupObject;
import com.github.seratch.jslack.api.model.block.composition.OptionObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.api.model.block.element.ButtonElement;
import com.github.seratch.jslack.api.model.block.element.StaticSelectElement;
import com.github.seratch.jslack.app_backend.interactive_messages.response.ActionResponse;
import org.apache.commons.lang3.StringUtils;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.persistence.GradeDao;
import ru.hh.school.lms.persistence.HomeworkDao;
import ru.hh.school.lms.persistence.LmsValueNotFoundException;
import ru.hh.school.lms.persistence.PersonDao;
import ru.hh.school.lms.persistence.entity.Grade;
import ru.hh.school.lms.persistence.entity.Homework;
import ru.hh.school.lms.persistence.entity.Person;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.blockaction.StaticSelectBlockAction;
import ru.hh.school.lms.slack.common.Attachments;
import ru.hh.school.lms.slack.endpoint.CallbackDispatcher;
import ru.hh.school.lms.slack.event.BlockActionEvent;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.flow.BaseFlow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GradeFlow extends BaseFlow {

  private static final String BLOCK_SELECT_HOMEWORK = "select_homework";
  private static final String BLOCK_SELECT_STUDENT = "select_student";
  private static final String BLOCK_SELECT_GRADE = "select_grade";

  private final SlackHelper slack;
  private final MessageManager message;
  private final PersonDao personDao;
  private final HomeworkDao homeworkDao;
  private final GradeDao gradeDao;

  public GradeFlow(
    SlackHelper slack,
    CallbackDispatcher dispatcher,
    MessageManager message,
    PersonDao personDao,
    HomeworkDao homeworkDao,
    GradeDao gradeDao
  ) {
    super(dispatcher);
    this.slack = slack;
    this.message = message;
    this.personDao = personDao;
    this.homeworkDao = homeworkDao;
    this.gradeDao = gradeDao;
  }

  public void show(BlockActionEvent event) {
    showFromContext(
      new GradeFlowContext(),
      event
    );
  }

  private void showFromContext(GradeFlowContext contextSource, BlockActionEvent event) {
    User lecturer = slack.getUser(event.getUser().getId())
      .orElseThrow(() -> new EventHandlingException("User from event was not found"));

    GradeFlowContext context = new GradeFlowContext(contextSource);

    List<Homework> homeworks = homeworkDao.getHomeworksSortedByDeadline();

    if (homeworks.isEmpty()) {
      postWarning(
        event.getResponseUrl(),
        event.getUser().getId(),
        message.of("data_homeworks_required")
      );
      return;
    }

    List<OptionObject> lecturerHomeworks = homeworks.stream()
      .filter(x -> lecturer.getProfile().getEmail().equals(x.getLecture().getLecturer().getEmail()))
      .map(x -> x.getLecture().getTitle())
      .filter(StringUtils::isNotEmpty)
      .distinct()
      .map(Attachments::stringToOptionObject)
      .collect(Collectors.toList());

    List<OptionObject> allHomeworks = homeworks.stream()
      .map(x -> x.getLecture().getTitle())
      .filter(StringUtils::isNotEmpty)
      .distinct()
      .map(Attachments::stringToOptionObject)
      .collect(Collectors.toList());

    List<OptionObject> availableStudents = personDao.getStudents().stream()
      .filter(x ->
        StringUtils.isNotEmpty(x.getEmail())
          && StringUtils.isNotEmpty(x.getFullName()))
      .map(
        x -> OptionObject.builder()
          .text(
            PlainTextObject.builder()
              .text(x.getFullName())
              .build())
          .value(x.getEmail())
          .build())
      .collect(Collectors.toList());

    if (availableStudents.isEmpty()) {
      showWarning(
        event.getResponseUrl(),
        context,
        message.of("data_students_required")
      );
      return;
    }

    SectionBlock titleBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(message.of("title_grade"))
        .build())
      .build();

    DividerBlock dividerBlock = DividerBlock.builder().build();

    String selectHomeworkCallbackId = attachCallback(BLOCK_SELECT_HOMEWORK, context, this::homeworkSelected);

    List<OptionGroupObject> homeworkListGroups = new ArrayList<>();
    if (!lecturerHomeworks.isEmpty()) {
      homeworkListGroups.add(OptionGroupObject.builder()
        .label(PlainTextObject.builder()
          .text(message.of("lecturer_homeworks_group_title"))
          .build())
        .options(lecturerHomeworks)
        .build());
    }
    homeworkListGroups.add(OptionGroupObject.builder()
      .label(PlainTextObject.builder()
        .text(message.of("all_homeworks_group_title"))
        .build())
      .options(allHomeworks)
      .build());

    SectionBlock homeworksBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(message.of("select_homework"))
        .build())
      .accessory(StaticSelectElement.builder()
        .placeholder(PlainTextObject.builder()
          .text(message.of("select_placeholder"))
          .build())
        .actionId(selectHomeworkCallbackId)
        .optionGroups(homeworkListGroups)
        .initialOption(
          allHomeworks.stream()
            .filter(x -> Objects.equals(x.getValue(), context.getHomework()))
            .findFirst()
            .orElse(null)
        )
        .build())
      .blockId(BLOCK_SELECT_HOMEWORK)
      .build();

    String selectStudentCallbackId = attachCallback(BLOCK_SELECT_STUDENT, context, this::studentSelected);

    SectionBlock studentsBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(message.of("select_student"))
        .build())
      .accessory(StaticSelectElement.builder()
        .placeholder(PlainTextObject.builder()
          .text(message.of("select_placeholder"))
          .build())
        .actionId(selectStudentCallbackId)
        .options(availableStudents)
        .build())
      .blockId(BLOCK_SELECT_STUDENT)
      .build();

    List<OptionObject> grades = IntStream.rangeClosed(1, 10)
      .mapToObj(
        x -> OptionObject.builder()
          .text(
            PlainTextObject.builder()
              .text(String.valueOf(x))
              .build())
          .value(String.valueOf(x))
          .build())
      .collect(Collectors.toList());
    Collections.reverse(grades);
    OptionObject initialGrade = grades.get(0);
    context.setGrade(Integer.valueOf(initialGrade.getValue()));

    String selectGradeCallbackId = attachCallback(BLOCK_SELECT_GRADE, context, this::gradeSelected);

    SectionBlock gradeBlock = SectionBlock.builder()
      .text(MarkdownTextObject.builder()
        .text(message.of("select_grade"))
        .build())
      .accessory(StaticSelectElement.builder()
        .placeholder(PlainTextObject.builder()
          .text(message.of("select_placeholder"))
          .build())
        .initialOption(initialGrade)
        .actionId(selectGradeCallbackId)
        .options(grades)
        .build())
      .blockId(BLOCK_SELECT_GRADE)
      .build();

    String submitCallbackId = attachCallback("submit", context, this::submitGrade);
    String cancelCallbackId = attachCallback("cancel", context, this::cancelGrade);

    ActionsBlock actionsBlock = ActionsBlock.builder()
      .elements(List.of(
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(message.of("submit"))
            .build())
          .actionId(submitCallbackId)
          .build(),
        ButtonElement.builder()
          .text(PlainTextObject.builder()
            .emoji(true)
            .text(message.of("cancel"))
            .build())
          .actionId(cancelCallbackId)
          .build()
      ))
      .build();

    context.setBlocks(
      List.of(
        titleBlock,
        dividerBlock,
        homeworksBlock,
        studentsBlock,
        gradeBlock,
        actionsBlock
      )
    );

    slack.respond(
      event.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(false)
        .blocks(context.getBlocks())
        .build()
    );
  }

  private void homeworkSelected(GradeFlowContext context, BlockActionEvent e) {
    StaticSelectBlockAction action = (StaticSelectBlockAction) e.getActions().get(0);
    String selectedValue = action.getSelectedOption().getValue();
    context.setHomework(selectedValue);
    context.setGradePrevious(fetchPreviousGrade(context.getStudent(), context.getHomework()));
    updateOnSelectionChanged(context, BLOCK_SELECT_HOMEWORK, e, selectedValue);
  }

  private void studentSelected(GradeFlowContext context, BlockActionEvent e) {
    StaticSelectBlockAction action = (StaticSelectBlockAction) e.getActions().get(0);
    String selectedValue = action.getSelectedOption().getValue();
    context.setStudent(selectedValue);
    context.setGradePrevious(fetchPreviousGrade(context.getStudent(), context.getHomework()));
    updateOnSelectionChanged(context, BLOCK_SELECT_STUDENT, e, selectedValue);
  }

  private void gradeSelected(GradeFlowContext context, BlockActionEvent e) {
    StaticSelectBlockAction action = (StaticSelectBlockAction) e.getActions().get(0);
    String selectedValue = action.getSelectedOption().getValue();
    context.setGrade(Integer.valueOf(selectedValue));
    updateOnSelectionChanged(context, BLOCK_SELECT_GRADE, e, selectedValue);
  }

  private void submitGrade(GradeFlowContext context, BlockActionEvent e) {
    if (context.getHomework() == null) {
      showWarning(e.getResponseUrl(), context, message.of("homework_required"));
    } else if (context.getStudent() == null) {
      showWarning(e.getResponseUrl(), context, message.of("student_required"));
    } else {
      try {
        gradeDao.saveGrade(context.getStudent(), context.getHomework(), context.getGrade());
      } catch (LmsValueNotFoundException exc) {
        showWarning(e.getResponseUrl(), context, message.of("homework_or_student_not_found"));
        return;
      }
      showSummary(context, e);
      showStudentNotification(context, e);

      dropContext(context);
    }
  }

  private void cancelGrade(GradeFlowContext context, BlockActionEvent e) {
    slack.respond(
      e.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .text(message.of("action_cancelled"))
        .build()
    );

    dropContext(context);
  }

  private void showWarning(String responseUrl, GradeFlowContext context, String msg) {
    String warning = message.of("warning_message", msg);
    slack.respond(
      responseUrl,
      ActionResponse.builder()
        .replaceOriginal(true)
        .attachments(List.of(Attachments.createWarning(warning)))
        .blocks(context.getBlocks())
        .build()
    );
  }

  private void postWarning(String responseUrl, String userId, String msg) {
    String warning = message.of("warning_message", msg);
    slack.respond(
      responseUrl,
      ActionResponse.builder()
        .replaceOriginal(false)
        .attachments(Collections.singletonList(Attachments.createWarning(warning)))
        .build()
    );
  }

  private void showSummary(GradeFlowContext context, BlockActionEvent e) {
    String studentName = personDao.getPersonByEmail(context.getStudent())
      .map(Person::getFullName)
      .orElseGet(context::getStudent);

    GradeFlowContext gradeAgainContext = new GradeFlowContext();
    gradeAgainContext.setHomework(context.getHomework());
    String gradeAgainCallbackId =
      attachCallback(
        "grade_again",
        gradeAgainContext,
        this::showFromContext
      );

    List<LayoutBlock> blocks =
      List.of(
        SectionBlock.builder()
          .text(MarkdownTextObject.builder()
            .text(message.of("title_grade_submitted"))
            .build())
          .build(),
        DividerBlock.builder().build(),
        SectionBlock.builder()
          .fields(List.of(
            MarkdownTextObject.builder()
              .text(message.of("selected_homework"))
              .build(),
            PlainTextObject.builder()
              .text(context.getHomework())
              .build(),
            MarkdownTextObject.builder()
              .text(message.of("selected_student"))
              .build(),
            PlainTextObject.builder()
              .text(studentName)
              .build(),
            MarkdownTextObject.builder()
              .text(message.of("selected_grade"))
              .build(),
            PlainTextObject.builder()
              .text(context.getGrade().toString())
              .build()
          ))
          .build(),
        ActionsBlock.builder()
          .elements(List.of(
            ButtonElement.builder()
              .text(PlainTextObject.builder()
                .emoji(true)
                .text(message.of("grade_again"))
                .build())
              .actionId(gradeAgainCallbackId)
              .build()
          ))
          .build()
      );

    slack.respond(
      e.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .blocks(blocks)
        .build()
    );
  }

  private void showStudentNotification(GradeFlowContext context, BlockActionEvent e) {
    if (context.getGrade().equals(context.getGradePrevious())) {
      // don't show notification if grade was not changed
      return;
    }
    Optional<User> user = slack.getUserByEmail(context.getStudent());
    if (user.isEmpty()) {
      postWarning(
        e.getResponseUrl(),
        e.getUser().getId(),
        message.of("student_not_found_in_chat", context.getStudent())
      );
      return;
    }
    String title = !context.isGradeChanged() ?
      message.of("title_grade_received", e.getUser().getId()) :
      message.of("title_grade_changed", e.getUser().getId(),
        context.getGradePrevious(), context.getGrade());
    slack.postIm(user.get().getId(), null, null,
      List.of(
        SectionBlock.builder()
          .text(MarkdownTextObject.builder()
            .text(title)
            .build())
          .build(),
        DividerBlock.builder().build(),
        SectionBlock.builder()
          .fields(List.of(
            MarkdownTextObject.builder()
              .text(message.of("selected_homework"))
              .build(),
            PlainTextObject.builder()
              .text(context.getHomework())
              .build(),
            MarkdownTextObject.builder()
              .text(message.of("selected_grade"))
              .build(),
            PlainTextObject.builder()
              .text(context.getGrade().toString())
              .build()
          ))
          .build()
      ));
  }

  private void updateOnSelectionChanged(GradeFlowContext context, String blockId, BlockActionEvent e, String value) {
    context.getBlocks().stream()
      .filter(block -> block instanceof SectionBlock)
      .map(block -> (SectionBlock) block)
      .filter(section -> Objects.equals(section.getBlockId(), blockId) &&
        section.getAccessory() instanceof StaticSelectElement)
      .forEach(section -> {
        StaticSelectElement select = ((StaticSelectElement) section.getAccessory());
        Optional<OptionObject> selectedOption = select.getOptions() != null ?
          select.getOptions().stream()
            .filter(x -> x.getValue().equals(value))
            .findFirst() :
          select.getOptionGroups().stream().flatMap(x -> x.getOptions().stream())
            .filter(x -> x.getValue().equals(value))
            .findFirst();
        selectedOption.ifPresent(select::setInitialOption);
      });

    List<Attachment> attachments = List.of();
    if (context.isGradeChanged()) {
      String warning = message.of(
        "warning_grade_changed",
        context.getGradePrevious(),
        context.getGrade()
      );
      attachments = List.of(Attachments.createWarning(warning));
    } else if (context.isGradeUnchanged()) {
      String warning = message.of(
        "warning_grade_unchanged",
        context.getGrade()
      );
      attachments = List.of(Attachments.createWarning(warning));
    }

    slack.respond(
      e.getResponseUrl(),
      ActionResponse.builder()
        .replaceOriginal(true)
        .attachments(attachments)
        .blocks(context.getBlocks())
        .build()
    );
  }

  private Integer fetchPreviousGrade(String student, String homework) {
    if (student == null || homework == null) {
      return null;
    }
    Optional<Grade> grade = gradeDao.getGradeByStudentByHomework(student, homework);
    return grade.map(Grade::getValue).orElse(null);
  }

}
