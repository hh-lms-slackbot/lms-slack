package ru.hh.school.lms.sheets;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.school.lms.persistence.sheets.relation.GoogleSheetRelation;
import ru.hh.school.lms.persistence.sheets.relation.GradesSheetRelation;
import ru.hh.school.lms.persistence.sheets.relation.HomeworksSheetRelation;
import ru.hh.school.lms.persistence.sheets.relation.LecturesSheetRelation;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@Configuration
@EnableScheduling
@ComponentScan("ru.hh.school.lms.persistence.sheets.relation.impl")
public class SheetsConfig {
  private static final String GOOGLE_SHEETS_APPLICATION_NAME = "Google Sheets App";
  private static final String GOOGLE_DRIVE_APPLICATION_NAME = "Google Drive App";

  @Bean
  SheetsClient googleSheetsClient(Sheets sheetsService, @Qualifier("serviceProperties") Properties settings) throws IOException {
    String spreadsheetId = settings.getProperty("ru.hh.school.lms.sheets.spreadsheetId");
    String dateFormat = settings.getProperty("sheets.date.format");
    return new SheetsClient(sheetsService, spreadsheetId, dateFormat);
  }

  @Bean
  Sheets googleSheets(FileSettings settings) throws GeneralSecurityException, IOException {
    return new Sheets.Builder(
      GoogleNetHttpTransport.newTrustedTransport(),
      JacksonFactory.getDefaultInstance(),
      credential(SheetsScopes.SPREADSHEETS, settings))
      .setApplicationName(GOOGLE_SHEETS_APPLICATION_NAME)
      .build();
  }

  @Bean
  Drive googleDrive(FileSettings settings) throws GeneralSecurityException, IOException {
    return new Drive.Builder(
      GoogleNetHttpTransport.newTrustedTransport(),
      JacksonFactory.getDefaultInstance(),
      credential(DriveScopes.DRIVE, settings))
      .setApplicationName(GOOGLE_DRIVE_APPLICATION_NAME)
      .build();
  }

  @Bean
  List<GoogleSheetRelation> orderedRelations(LecturesSheetRelation lecturesRelation,
                                             HomeworksSheetRelation homeworksRelation,
                                             GradesSheetRelation gradesSheetRelation) {
    return Arrays.asList(lecturesRelation, homeworksRelation, gradesSheetRelation);
  }


  Credential credential(String scopes, FileSettings settings) throws IOException {
    final String credentialFile = settings.getString("ru.hh.school.lms.sheets.googleApiCredential");
    InputStream is = SheetsConfig.class.getResourceAsStream(credentialFile);
    return GoogleCredential.fromStream(is).createScoped(Collections.singleton(scopes));
  }

}
