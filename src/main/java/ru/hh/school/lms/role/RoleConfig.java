package ru.hh.school.lms.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import ru.hh.school.lms.MessageManager;
import ru.hh.school.lms.slack.SlackHelper;

import java.util.Locale;
import java.util.Properties;

@Configuration
class RoleConfig {

  @Autowired
  private Locale locale;

  @Bean(name = "roleMessage")
  public MessageManager message() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("role/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return (messageCode, args) -> messageSource.getMessage(messageCode, args, locale);
  }

  @Bean
  ChannelRoleResolver resolver(SlackHelper slack, @Qualifier("serviceProperties") Properties settings) {
    return new ChannelRoleResolver(slack, settings);
  }
}
