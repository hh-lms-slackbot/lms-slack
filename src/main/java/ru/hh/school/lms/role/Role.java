package ru.hh.school.lms.role;

public enum Role {
  STUDENT,
  LECTURER,
  ADMIN
}
