package ru.hh.school.lms.role;

import java.util.Set;

public interface RoleResolver {
  Set<Role> resolve(String userId);
}
