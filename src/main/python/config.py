CREDENTIALS_FILE = 'hh-lms-google-private-key.json'  # path to Google Sheets/Drive API private key
EMAIL = 'email-to-provide-write-access@example.com'  # email address that will be able to edit sheets
