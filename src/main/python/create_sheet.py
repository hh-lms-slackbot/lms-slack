# pip install --upgrade google-api-python-client
# pip install oauth2client

from pprint import pprint

import apiclient.discovery
import httplib2
from config import CREDENTIALS_FILE, EMAIL
from oauth2client.service_account import ServiceAccountCredentials

if __name__ == "__main__":
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE,
                                                                   ['https://www.googleapis.com/auth/spreadsheets',
                                                                    'https://www.googleapis.com/auth/drive'])
    httpAuth = credentials.authorize(httplib2.Http())
    service = apiclient.discovery.build('sheets', 'v4', http=httpAuth)

    your_document_name = 'Document tables'
    first_list_name = 'List (sheetId: 0)'

    spreadsheet = service.spreadsheets().create(body={
        'properties': {'title': your_document_name, 'locale': 'ru_RU'},
        'sheets': [{'properties': {'sheetType': 'GRID',
                                   'sheetId': 0,
                                   'title': first_list_name,
                                   'gridProperties': {'rowCount': 8, 'columnCount': 5}}}]  # тут параметры вашей таблицы
    }).execute()

    spreadsheetId = spreadsheet['spreadsheetId']
    pprint(spreadsheet)

    print("\nYour spreadsheet id:" + spreadsheetId + "\n")

    driveService = apiclient.discovery.build('drive', 'v3', http=httpAuth)
    shareRes = driveService.permissions().create(
        fileId=spreadsheetId,
        body={'type': 'user', 'role': 'writer', 'emailAddress': EMAIL},
        fields='id'
    ).execute()
